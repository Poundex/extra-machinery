package net.poundex.xmachinery.spring.typedmongo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoManagedTypes;

@Configuration
class ContextConfiguration {
	@Bean
	public CollectionTypesRegistry collectionTypesRegistry(MongoManagedTypes mongoManagedTypes) {
		return new CollectionTypesRegistry(mongoManagedTypes);
	}
}
