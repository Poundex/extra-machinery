package net.poundex.xmachinery.spring.typedmongo;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.mongodb.MongoManagedTypes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

@RequiredArgsConstructor
class CollectionTypesRegistry implements ApplicationListener<ContextRefreshedEvent> {

	private final MongoManagedTypes mongoManagedTypes;

	private record ClassNode(Class<?> klass, Set<ClassNode> subclasses) {
		private ClassNode(Class<?> klass) {
			this(klass, new HashSet<>());
		}
		
		Stream<Class<?>> andAllSubclasses() {
			return Stream.concat(Stream.of(klass()), 
					subclasses().stream().flatMap(ClassNode::andAllSubclasses));
		}
	}
	private final Map<Class<?>, ClassNode> classNodes = new HashMap<>();
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent ignored) {
		mongoManagedTypes.forEach(this::addClass);
	}

	public void addClass(Class<?> klass) {
		ClassNode classNode = new ClassNode(klass);
		classNodes.put(klass, classNode);

		Class<?> superclass = klass.getSuperclass();
		if( ! superclass.isAnnotationPresent(Document.class))
			return;

		if( ! classNodes.containsKey(superclass))
			classNodes.put(superclass, new ClassNode(superclass));

		classNodes.get(superclass).subclasses().add(classNode);
	}
	
	public List<String> classCollectionFor(Class<?> dboType) {
		return classNodes.get(dboType).andAllSubclasses().map(Class::getName).toList();
	}
}
