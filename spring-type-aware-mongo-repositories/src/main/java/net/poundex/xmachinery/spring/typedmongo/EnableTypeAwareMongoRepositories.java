package net.poundex.xmachinery.spring.typedmongo;

import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(ElementType.TYPE)
@EnableReactiveMongoRepositories(repositoryFactoryBeanClass = TypeAwareMongoRepositoryFactoryBean.class)
@Import(ContextConfiguration.class)
public @interface EnableTypeAwareMongoRepositories {
}
