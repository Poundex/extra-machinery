package net.poundex.xmachinery.spring.typedmongo;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;
import net.poundex.xmachinery.utils.ReflectionUtil;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.core.ResolvableType;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.support.ReactiveMongoRepositoryFactoryBean;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import java.io.Serializable;
import java.util.Arrays;

class TypeAwareMongoRepositoryFactoryBean<T extends Repository<S, ID>, S, ID extends Serializable> extends ReactiveMongoRepositoryFactoryBean<T, S, ID> {
	
	private final CollectionTypesRegistry registry;

	public TypeAwareMongoRepositoryFactoryBean(Class<? extends T> repositoryInterface, CollectionTypesRegistry registry) {
		super(repositoryInterface);
		this.registry = registry;
	}

	@Override
	protected RepositoryFactorySupport getFactoryInstance(ReactiveMongoOperations operations) {
		Class<?> dboType = ReflectionUtil.findGenericSuperInterface(ResolvableType
						.forType(getObjectType()), Repository.class)
				.getGeneric(0)
				.getRawClass();
		
		ProxyFactory pf = new ProxyFactory(operations);
		pf.addAdvice((MethodInterceptor) invocation ->
				invocation.getMethod().invoke(invocation.getThis(),
						Arrays.stream(invocation.getArguments())
								.map(arg -> arg instanceof Query q
										? q.addCriteria(Criteria.where("_class")
										.in(registry.classCollectionFor(dboType)))
										: arg)
								.toArray()));


		RepositoryFactorySupport factoryInstance = super.getFactoryInstance((ReactiveMongoOperations) pf.getProxy());

		factoryInstance.addRepositoryProxyPostProcessor((factory, repositoryInformation) ->
				factory.addAdvice((MethodInterceptor) invocation -> {
					((ProxyMethodInvocation) invocation).setArguments(
							Arrays.stream(invocation.getArguments())
									.map(arg -> arg instanceof Predicate p
											? Expressions.stringPath("_class")
												.in(registry.classCollectionFor(dboType))
												.and(p)
											: arg)
									.toArray());
					return invocation.proceed();
				}));
		return factoryInstance;
	}

}
