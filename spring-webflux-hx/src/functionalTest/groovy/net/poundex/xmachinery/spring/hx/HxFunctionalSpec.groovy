package net.poundex.xmachinery.spring.hx


import groovy.transform.TupleConstructor
import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import spock.lang.Specification

@SpringBootTest(classes = FunctionalTestApplication, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HxFunctionalSpec extends Specification implements PublisherUtils {
	
	@Autowired
	WebClient webClient
	
	@TupleConstructor
	class ResponseWrapper {
		ClientResponse response
		String body
	}
	
	void "Returns correct decorated web response"() {
		when:
		ResponseWrapper response = block webClient
				.get()
				.exchangeToMono(resp -> resp
						.bodyToMono(String)
						.map { body -> new ResponseWrapper(resp, body) })
		
		then: "Response is HTML"
		response.response.headers().contentType().get() == MediaType.TEXT_HTML
		
		and: "Response is decorated"
		response.body.contains('<div class="decorator_content_container">')
		
		and: "Response includes content"
		response.body.contains("Index")
	}

	void "Returns correct fragment web response"() {
		when:
		ResponseWrapper response = block webClient
				.get()
				.header(Htmx.HEADER_HX_REQUEST, "true")
				.exchangeToMono(resp -> resp
						.bodyToMono(String)
						.map { body -> new ResponseWrapper(resp, body) })

		then: "Response is HTML"
		response.response.headers().contentType().get() == MediaType.TEXT_HTML

		and: "Response is not decorated"
		! response.body.contains('<div class="decorator_content_container">')

		and: "Response includes content"
		response.body.contains("Index")
	}

	void "Returns correct decorated mobile response"() {
		when:
		ResponseWrapper response = block webClient
				.get()
				.header(HttpHeaders.ACCEPT, "application/vnd.hyperview+xml")
				.exchangeToMono(resp -> resp
						.bodyToMono(String)
						.map { body -> new ResponseWrapper(resp, body) })

		then: "Response is HXML"
		response.response.headers().contentType().get().toString() == "application/vnd.hyperview+xml"

		and: "Response is decorated"
		response.body.contains('<view id="decorator_content_container">')

		and: "Response includes content"
		response.body.contains("Index")
	}

	void "Returns correct decorated mobile response"() {
		when:
		ResponseWrapper response = block webClient
				.get()
				.header(HttpHeaders.ACCEPT, "application/vnd.hyperview_fragment+xml")
				.exchangeToMono(resp -> resp
						.bodyToMono(String)
						.map { body -> new ResponseWrapper(resp, body) })

		then: "Response is HXML"
		response.response.headers().contentType().get().toString() == "application/vnd.hyperview_fragment+xml"

		and: "Response is not decorated"
		! response.body.contains('<view id="decorator_content_container">')

		and: "Response includes content"
		response.body.contains("Index")
	}
}
