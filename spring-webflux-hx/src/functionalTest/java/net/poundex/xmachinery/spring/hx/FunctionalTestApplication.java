package net.poundex.xmachinery.spring.hx;

import nz.net.ultraq.thymeleaf.layoutdialect.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;

@SpringBootApplication
@Controller
@RequestMapping("/")
class FunctionalTestApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(FunctionalTestApplication.class, args);
	}
	
	@GetMapping
	public Flux<Rendering> index(Hx hx) {
		return hx.render("index");
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}
	
	@Bean
	@Lazy
	public WebClient webClient(WebClient.Builder clientBuilder, @LocalServerPort int port) {
		return clientBuilder.baseUrl("http://localhost:" + port + "/").build();
	}
}
