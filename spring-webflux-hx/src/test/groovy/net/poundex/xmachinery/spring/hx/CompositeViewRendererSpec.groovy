package net.poundex.xmachinery.spring.hx

import org.springframework.core.ResolvableType
import org.springframework.web.reactive.HandlerResult
import org.springframework.web.reactive.result.view.Rendering
import org.springframework.web.reactive.result.view.ViewResolver
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class CompositeViewRendererSpec extends Specification {
	
	ViewResolver viewResolver = Stub()
	
	@Subject
	CompositeViewRenderer viewRenderer = new CompositeViewRenderer(viewResolver)
	
	void "Supports Publishers of Renderings"(ResolvableType returnType, boolean expected) {
		given:
		HandlerResult handlerResult = Stub() {
			getReturnType() >> returnType
		}
		
		expect:
		viewRenderer.supports(handlerResult) == expected
		
		where:
		returnType                                           || expected
		ResolvableType.forClass(Object)                      || false
		ResolvableType.forClassWithGenerics(Mono, Object)    || false
		ResolvableType.forClassWithGenerics(Mono, Rendering) || true
		ResolvableType.forClassWithGenerics(Flux, Rendering) || true
	}
}
