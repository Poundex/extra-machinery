package net.poundex.xmachinery.spring.hx

import org.springframework.core.MethodParameter
import org.springframework.http.HttpHeaders
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.server.ServerWebExchange
import spock.lang.Specification
import spock.lang.Subject

class HxArgumentResolverSpec extends Specification {
	
	HxProperties properties = Stub()
	
	@Subject
	HxArgumentResolver resolver = new HxArgumentResolver(properties)
	
	void "Assignable to Hx parameter"() {
		given:
		MethodParameter param = Stub() {
			getParameterType() >> Hx
		}
		
		expect:
		resolver.supportsParameter(param)
	}
	
	void "Returns correct argument value"(String header, Class<? extends Hx> exptected) {
		given:
		ServerWebExchange exchange = Stub() {
			getRequest() >> Stub(ServerHttpRequest) {
				getHeaders() >> Stub(HttpHeaders) {
					get(HttpHeaders.ACCEPT) >> [ header ]
				}
			}
		}

		when:
		Hx argValue = resolver.resolveArgumentValue(null, null, exchange)
		
		then:
		argValue
		exptected.isInstance(argValue)
		
		where:
		header                               || exptected
		"application/vnd.hyperview"          || Hxml
		"application/vnd.hyperview_fragment" || Hxml
		"text/html"                          || Htmx
		"*/*"                                || Htmx
	}
}
