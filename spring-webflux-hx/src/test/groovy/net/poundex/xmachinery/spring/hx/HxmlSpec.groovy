package net.poundex.xmachinery.spring.hx

import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.context.i18n.LocaleContext
import org.springframework.http.HttpHeaders
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.util.MultiValueMap
import org.springframework.web.reactive.result.view.Rendering
import org.springframework.web.server.ServerWebExchange
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Supplier

class HxmlSpec extends Specification implements PublisherUtils {


	HxProperties props = Stub() {
		getEmptyLayoutName() >> "empty-layout-name"
		getDefaultLayoutName() >> "default-layout-name"
	}

	HttpHeaders requestHeaders = Stub()
	URI requestUri = "/request-uri".toURI()
	MultiValueMap<String, String> requestQueryParams = Stub()
	
	HttpHeaders responseHeaders = Mock()

	ServerWebExchange exchange = Stub() {
		getLocaleContext() >> Stub(LocaleContext) {
			getLocale() >> null
		}

		getRequest() >> Stub(ServerHttpRequest) {
			getURI() >> { requestUri }
			getHeaders() >> { requestHeaders }
			getQueryParams() >> { requestQueryParams }
		}
		
		getResponse() >> Stub(ServerHttpResponse) {
			getHeaders() >> { responseHeaders }
		}
	}

	@Subject
	Hxml hxml = new Hxml(exchange, props)

	void "Includes model attributes"() {
		when:
		hxml.modelAttribute("k1", "v1")
		hxml.modelAttribute("k2", "v2")
		List<Rendering> renderings = block hxml.render("one")

		then:
		with(renderings.first()) {
			it.modelAttributes()['k1'] == "v1"
			it.modelAttributes()['k2'] == "v2"
		}
	}

	void "Computes model attributes"() {
		when:
		hxml.modelAttribute("key", "v1")
		hxml.computeModelAttribute("key", v -> v + "v2")
		List<Rendering> renderings = block hxml.render("one")

		then:
		with(renderings.first()) {
			it.modelAttributes()['key'] == "v1v2"
		}
	}

	void "Returns mobile value for getValue"() {
		Supplier<String> web = Mock()
		Supplier<String> mobile = Mock()

		when:
		String r = hxml.getValue(web, mobile)

		then:
		1 * mobile.get() >> "the-string"
		0 * web.get()

		and:
		r == "the-string"
	}

	void "Includes triggers in model"() {
		when:
		hxml.trigger("event-1")
		hxml.trigger("event-2")
		List<Rendering> render = block hxml.render("one")

		then:
		with(render.first()) {
			it.modelAttributes()['triggers'] == ['event-1', 'event-2'].toSet()
		}
	}

	void "Returns whether request is hypermedia request - from query"() {
		expect:
		! hxml.isHypermediaRequest()
		
		when:
		requestQueryParams.containsKey("HX") >> true

		then:
		hxml.isHypermediaRequest()
	}

	void "Returns whether request is hypermedia request - from accept header"() {
		expect:
		!hxml.isHypermediaRequest()

		when:
		requestHeaders.get(HttpHeaders.ACCEPT) >> ["application/vnd.hyperview_fragment"]

		then:
		hxml.isHypermediaRequest()
	}

	void "Renders views correctly"() {
		when:
		List<Rendering> renderings = block hxml.render("one", "two")

		then: "Views are correct"
		renderings.find { it.view() as String == "one.xml" }
		renderings.find { it.view() as String == "two.xml" }

		and: "Common model attributes are added"
		renderings.every { r ->
			with(r) {
				it.modelAttributes().containsKey("locale")
				it.modelAttributes()['requestUri'] == "/request-uri?"
				it.modelAttributes().containsKey("requestQuery")
				it.modelAttributes()['layout'] == "default-layout-name"
				! it.modelAttributes().containsKey("flash")
			}
			true
		}
		
		and: "Response type is correct"
		1 * exchange.getResponse().getHeaders().set(HttpHeaders.CONTENT_TYPE, "application/vnd.hyperview+xml")
	}

	
	void "Sets correct content type for hypermedia request"() {
		given:
		requestQueryParams.containsKey("HX") >> true

		when:
		block hxml.render("one")

		then:
		1 * exchange.getResponse().getHeaders().set(HttpHeaders.CONTENT_TYPE, "application/vnd.hyperview_fragment+xml")
	}

	void "Includes request query when present"() {
		given:
		requestUri = "/request-uri?k1=v1&k2=v2".toURI()

		when:
		List<Rendering> renderings = block hxml.render("one")

		then:
		with(renderings.first()) {
			it.modelAttributes()['requestQuery'] == "k1=v1&k2=v2"
		}
	}

	void "Uses empty layout on hypermedia request"() {
		given:
		requestQueryParams.containsKey("HX") >> true

		when:
		List<Rendering> renderings = block hxml.render("one")

		then:
		with(renderings.first()) {
			it.modelAttributes()['layout'] == "empty-layout-name"
		}
	}

	void "Includes flash items when present"() {
		when:
		hxml.flash("s1", "m1")
		hxml.flash("s2", "m2")
		List<Rendering> renderings = block hxml.render("one")

		then:
		with(renderings.first()) {
			with(it.modelAttributes()['flash'] as List<HypermediaResponseSupport.FlashItem>) { f ->
				f.size() == 2
				f.find { it.style() == "s1" && it.message() == "m1" }
				f.find { it.style() == "s2" && it.message() == "m2" }
			}
		}
	}
}
