package net.poundex.xmachinery.spring.hx

import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.context.i18n.LocaleContext
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.reactive.result.view.Rendering
import org.springframework.web.server.ServerWebExchange
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Supplier

class HtmxSpec extends Specification implements PublisherUtils {

	HxProperties props = Stub() {
		getEmptyLayoutName() >> "empty-layout-name"
		getDefaultLayoutName() >> "default-layout-name"
	}
	
	HttpHeaders requestHeaders = Stub()
	URI requestUri = "/request-uri".toURI()
	
	ServerWebExchange exchange = Stub() {
		getLocaleContext() >> Stub(LocaleContext) {
			getLocale() >> null
		}

		getRequest() >> Stub(ServerHttpRequest) {
			getURI() >> { requestUri }
			getHeaders() >> { requestHeaders }
		}
	}

	@Subject
	Htmx htmx = new Htmx(props, exchange)
	
	void "Includes model attributes"() {
		when:
		htmx.modelAttribute("k1", "v1")
		htmx.modelAttribute("k2", "v2")
		List<Rendering> renderings = block htmx.render("one")
		
		then:
		with(renderings.first()) {
			it.modelAttributes()['k1'] == "v1"
			it.modelAttributes()['k2'] == "v2"
		}
	}
	
	void "Sets custom decorator layout"() {
		when:
		htmx.decorate("custom-layout")
		List<Rendering> renderings = block htmx.render("one")
		
		then:
		with(renderings.first()) {
			it.modelAttributes()['layout'] == "custom-layout"
		}
	}
	
	void "Computes model attributes"() {
		when:
		htmx.modelAttribute("key", "v1")
		htmx.computeModelAttribute("key", v -> v + "v2")
		List<Rendering> renderings = block htmx.render("one")
		
		then:
		with(renderings.first()) {
			it.modelAttributes()['key'] == "v1v2"
		}
	}

	void "Sends Push URL header"() {
		when:
		htmx.pushUrl("the-url")
		List<Rendering> render = block htmx.render("one", "two")

		then:
		render.every {
			it.headers().getFirst(Htmx.HEADER_HX_PUSH_URL) == "the-url"
		}
	}

	void "Sets response status"() {
		when:
		htmx.status(HttpStatus.CONFLICT)
		List<Rendering> render = block htmx.render("one", "two")

		then:
		render.every {
			it.status() == HttpStatus.CONFLICT
		}
	}

	void "Returns web value for getValue"() {
		Supplier<String> web = Mock()
		Supplier<String> mobile = Mock()

		when:
		String r = htmx.getValue(web, mobile)

		then:
		1 * web.get() >> "the-string"
		0 * mobile.get()

		and:
		r == "the-string"
	}

	void "Sends out of band header when requested"() {
		when:
		htmx.outOfBand()
		List<Rendering> render = block htmx.render("one", "two")

		then:
		render.every {
			it.modelAttributes().get("swapAttr") == "hx-swap-oob=true"
		}
	}

	void "Sends dummy swap attribute when not out of band"() {
		when:
		List<Rendering> render = block htmx.render("one", "two")

		then:
		render.every {
			! it.modelAttributes().get("swapAttr").toString().contains("hx-swap-oob")
		}
	}

	void "Sends Trigger header"() {
		when:
		htmx.trigger("the-event")
		List<Rendering> render = block htmx.render("one", "two")

		then:
		render.every {
			it.headers().getFirst(Htmx.HEADER_HX_TRIGGER) == "the-event"
		}
	}

	void "Returns whether request is hypermedia request"(String headerValue, boolean expected) {
		given:
		requestHeaders.getFirst(Htmx.HEADER_HX_REQUEST) >> headerValue

		expect:
		htmx.isHypermediaRequest() == expected
		
		where:
		headerValue || expected
		null        || false
		"false"     || false
		"true"      || true
	}
	
	void "Renders views correctly"() {
		when:
		List<Rendering> renderings = block htmx.render("one", "two")
		
		then: "Views are correct"
		renderings.find { it.view() as String == "one" }
		renderings.find { it.view() as String == "two" }
		
		and: "Common model attributes are added"
		renderings.every {r ->
			with(r) {
				it.modelAttributes().containsKey("locale")
				it.modelAttributes()['requestUri'] == "/request-uri"
				it.modelAttributes().containsKey("requestQuery")
				it.modelAttributes()['layout'] == "default-layout-name"
				! it.modelAttributes().containsKey("flash")
			}
			true
		}
	}
	
	void "Includes request query when present"() {
		given:
		requestUri = "/request-uri?k1=v1&k2=v2".toURI()

		when:
		List<Rendering> renderings = block htmx.render("one")
		
		then:
		with(renderings.first()) {
			it.modelAttributes()['requestQuery'] == "k1=v1&k2=v2"
		}
	}
	
	void "Uses empty layout on hypermedia request"() {
		given:
		requestHeaders.getFirst(Htmx.HEADER_HX_REQUEST) >> "true"

		when:
		List<Rendering> renderings = block htmx.render("one")
		
		then:
		with(renderings.first()) {
			it.modelAttributes()['layout'] == "empty-layout-name"
		}
	}
	
	void "Includes flash items when present"() {
		when:
		htmx.flash("s1", "m1")
		htmx.flash("s2", "m2")
		List<Rendering> renderings = block htmx.render("one")
		
		then:
		with(renderings.first()) {
			with(it.modelAttributes()['flash'] as List<HypermediaResponseSupport.FlashItem>) { f ->
				f.size() == 2
				f.find { it.style() == "s1" && it.message() == "m1" }
				f.find { it.style() == "s2" && it.message() == "m2" }
			}
		}
	}
}