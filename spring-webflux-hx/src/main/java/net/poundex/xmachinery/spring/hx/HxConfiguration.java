package net.poundex.xmachinery.spring.hx;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.result.view.ViewResolver;

@Configuration
@EnableConfigurationProperties(HxProperties.class)
class HxConfiguration {
	@Bean
	public CompositeViewRenderer compositeViewRenderer(ViewResolver viewResolver) {
		return new CompositeViewRenderer(viewResolver);
	}
	
	@Bean
	public HxArgumentResolver argumentResolver(HxProperties properties) {
		return new HxArgumentResolver(properties);
	}
}
