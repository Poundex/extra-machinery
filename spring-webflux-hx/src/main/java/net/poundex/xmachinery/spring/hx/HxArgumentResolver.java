package net.poundex.xmachinery.spring.hx;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.SyncHandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
class HxArgumentResolver implements SyncHandlerMethodArgumentResolver {
	
	private final HxProperties hxProperties;
	
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return Hx.class.isAssignableFrom(parameter.getParameterType());
	}

	@Override
	public Hx resolveArgumentValue(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
		return Optional.ofNullable(exchange.getRequest().getHeaders().get(HttpHeaders.ACCEPT)).stream()
				.flatMap(Collection::stream)
				.filter(mt -> mt.contains("application/vnd.hyperview"))
				.findFirst()
				.<Hx>map(ignored -> new Hxml(exchange, hxProperties))
				.orElseGet(() -> new Htmx(hxProperties, exchange));
	}
}
