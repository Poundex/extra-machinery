package net.poundex.xmachinery.spring.hx;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.HandlerResult;
import org.springframework.web.reactive.HandlerResultHandler;
import org.springframework.web.reactive.result.method.InvocableHandlerMethod;
import org.springframework.web.reactive.result.view.Rendering;
import org.springframework.web.reactive.result.view.View;
import org.springframework.web.reactive.result.view.ViewResolver;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.ServerWebExchangeDecorator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
@RequiredArgsConstructor
/* https://github.com/dsyer/spring-todo-mvc/blob/main/src/main/java/example/todomvc/web/CompositeViewRenderer.java */
class CompositeViewRenderer implements HandlerResultHandler {
    
    private final ViewResolver resolver;

    @Override
    public boolean supports(HandlerResult result) {
        if (Publisher.class.isAssignableFrom(result.getReturnType().toClass())) 
            return Rendering.class.isAssignableFrom(result.getReturnType().getGeneric(0).toClass());
        
        return false;
    }

    @Override
    public Mono<Void> handleResult(ServerWebExchange exchange, HandlerResult result) {
        String[] methodAnnotation = ((InvocableHandlerMethod) result.getHandler())
                .getMethodAnnotation(RequestMapping.class).produces();
        
        MediaType earlyType = methodAnnotation.length > 0 
                ? MediaType.valueOf(methodAnnotation[0]) 
                : MediaType.TEXT_HTML;
        
        boolean sse = MediaType.TEXT_EVENT_STREAM.includes(earlyType);
        @SuppressWarnings("unchecked")
        Flux<Rendering> renderings = Flux.from((Publisher<Rendering>) result.getReturnValue());
        
        ExchangeWrapper wrapper = new ExchangeWrapper(exchange);
        return exchange.getResponse().writeAndFlushWith(render(wrapper, renderings, earlyType)
                .map(buffers -> transform(exchange.getResponse().bufferFactory(), buffers, sse)));
    }

    private Publisher<DataBuffer> transform(DataBufferFactory factory, Flux<DataBuffer> buffers, boolean sse) {
        if (sse) 
            buffers = buffers.map(buffer -> prefix(buffer, factory.allocateBuffer(buffer.capacity())));
        
        // Add closing empty lines
        return buffers.map(buffer -> buffer.write("\n\n", StandardCharsets.UTF_8));
    }

    private DataBuffer prefix(DataBuffer buffer, DataBuffer result) {
        String body = buffer.toString(StandardCharsets.UTF_8);
        body = "data:" + body.replace("\n", "\ndata:");
        result.write(body, StandardCharsets.UTF_8);
        DataBufferUtils.release(buffer);
        return result;
    }

    private Flux<Flux<DataBuffer>> render(ExchangeWrapper exchange, Flux<Rendering> renderings, MediaType earlyType) {
        return renderings.flatMap(rendering -> render(exchange, rendering, earlyType))
                .contextWrite(view -> view.put("body", new AtomicReference<Flux<Flux<DataBuffer>>>(Flux.empty())));
    }

    private Flux<Flux<DataBuffer>> render(ExchangeWrapper exchange, Rendering rendering, MediaType earlyType) {
        MediaType type = Optional.ofNullable(
                        exchange.getResponse().getHeaders().getFirst(HttpHeaders.CONTENT_TYPE))
                .map(MediaType::valueOf)
                .orElse(earlyType);

        Mono<View> view = (rendering.view() instanceof View v)
                ? Mono.just(v)
                : resolveView(exchange, rendering);

        exchange.getResponse().getHeaders().addAll(rendering.headers());
        exchange.getResponse().setStatusCode(rendering.status());
        log.debug("View: {}", rendering.view());
        return view
                .flatMap(actual -> actual.render(rendering.modelAttributes(), type, exchange)
                        .then(Mono.fromRunnable(() -> exchange.response.getHeaders().setContentType(type))))
                .thenMany(Flux.deferContextual(context -> {
                    @SuppressWarnings("unchecked")
                    Flux<Flux<DataBuffer>> flux = ((AtomicReference<Flux<Flux<DataBuffer>>>) context.get("body"))
                            .getAndSet(Flux.empty());
                    return flux;
                }));
    }

    private Mono<View> resolveView(ServerWebExchange exchange, Rendering rendering) {
        Locale locale = exchange.getLocaleContext().getLocale();
        if (locale == null) locale = Locale.getDefault();
        
        return resolver.resolveViewName((String) rendering.view(), locale);
    }

    @Getter
    static class ExchangeWrapper extends ServerWebExchangeDecorator {

        private final ResponseWrapper response;

        protected ExchangeWrapper(ServerWebExchange delegate) {
            super(delegate);
            this.response = new ResponseWrapper(super.getResponse());
        }
    }

    @Getter
    static class ResponseWrapper extends ServerHttpResponseDecorator {

        private final HttpHeaders headers;

        public ResponseWrapper(ServerHttpResponse delegate) {
            super(delegate);
            headers = HttpHeaders.writableHttpHeaders(delegate.getHeaders());
        }

        @Override
        public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
            return writeAndFlushWith(Mono.just(body));
        }

        @Override
        public Mono<Void> writeAndFlushWith(Publisher<? extends Publisher<? extends DataBuffer>> body) {
            Flux<Flux<DataBuffer>> map = Flux.from(body).map(Flux::from);
            return Mono.deferContextual(context -> {
                AtomicReference<Flux<Flux<DataBuffer>>> flux = context.get("body");
                return flux.updateAndGet(buffer -> {
                            log.debug("Appending: {}", map);
                            return buffer.concatWith(map);
                        })
                        .then();
            });
        }
    }
}
