package net.poundex.xmachinery.spring.hx;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.server.ServerWebExchange;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.UnaryOperator;

@RequiredArgsConstructor
abstract class HypermediaResponseSupport implements Hx {
	
	public record FlashItem(String style, String message) { }
	
	protected final ServerWebExchange exchange;

	protected final Map<String, Object> model = new HashMap<>();
	protected final List<FlashItem> flash = new ArrayList<>();
	protected final HttpHeaders headers = new HttpHeaders();
	protected Optional<String> layoutName = Optional.empty();

	@Override
	public Hx modelAttribute(String name, Object value) {
		model.put(name, value);
		return this;
	}

	@Override
	public Locale getLocale() {
		// TODO: Null locale
		return Optional.ofNullable(exchange.getLocaleContext().getLocale())
				.orElse(Locale.UK);
	}

	@Override
	public Hx flash(String style, String message) {
		this.flash.add(new FlashItem(style, message));
		return this;
	}

	@Override
	public Hx refresh() {
		this.headers.add("HX-Refresh", "true");
		return this;
	}

	@Override
	public Hx decorate(String layoutName) {
		this.layoutName = Optional.of(layoutName);
		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Hx computeModelAttribute(String name, UnaryOperator<T> mapper) {
		if(model.containsKey(name))
			model.put(name, mapper.apply((T) model.get(name)));
		
		return this;
	}
}
