package net.poundex.xmachinery.spring.hx;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("hx")
@Data
class HxProperties {
	private final String emptyLayoutName;
	private final String defaultLayoutName;
}
