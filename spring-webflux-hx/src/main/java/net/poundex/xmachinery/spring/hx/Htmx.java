package net.poundex.xmachinery.spring.hx;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.reactive.result.view.Rendering;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

class Htmx extends HypermediaResponseSupport implements Hx {

	public static final String HEADER_HX_REQUEST = "HX-Request";
	public static final String HEADER_HX_PUSH_URL = "HX-Push-Url";
	public static final String HEADER_HX_TRIGGER = "HX-Trigger";

	private final HxProperties hxProperties;
	
	private boolean outOfBand = false;
	protected HttpStatusCode httpStatus = HttpStatus.OK;

	public Htmx(HxProperties hxProperties, ServerWebExchange exchange) {
		super(exchange);
		this.hxProperties = hxProperties;
	}

	@Override
	public Htmx pushUrl(String url) {
		this.headers.add(HEADER_HX_PUSH_URL, url);
		return this;
	}

	@Override
	public Htmx status(HttpStatusCode httpStatus) {
		this.httpStatus = httpStatus;
		return this;
	}

	@Override
	public <T> T getValue(Supplier<T> web, Supplier<T> mobile) {
		return web.get();
	}

	@Override
	public Htmx outOfBand() {
		this.outOfBand = true;
		return this;
	}

	@Override
	public Hx trigger(String event) {
		this.headers.add(HEADER_HX_TRIGGER, event);
		return this;
	}

	@Override
	public boolean isHypermediaRequest() {
		return Objects.equals(exchange.getRequest().getHeaders().getFirst(HEADER_HX_REQUEST), "true");
	}

	@Override
	public Flux<Rendering> render(String... views) {
		completeModel();
		modelAttribute("swapAttr", outOfBand ? "hx-swap-oob=true" : "data-nothing=nothing");
		return Flux.fromArray(views)
				.map(Rendering::view)
				.map(r -> r.model(model).headers(headers).status(httpStatus).build());
	}

	private void completeModel() {
		modelAttribute("locale", getLocale());
		modelAttribute("requestUri", exchange.getRequest().getURI().toString());
		modelAttribute("requestQuery", Optional.ofNullable(exchange.getRequest().getURI().getQuery()).orElse(""));
		modelAttribute("layout", isHypermediaRequest()
				? Optional.ofNullable(hxProperties.getEmptyLayoutName()).orElse("empty")
				: layoutName.or(() -> Optional.ofNullable(hxProperties.getDefaultLayoutName())).orElse("main"));

		if ( ! flash.isEmpty()) modelAttribute("flash", flash);
	}
}
