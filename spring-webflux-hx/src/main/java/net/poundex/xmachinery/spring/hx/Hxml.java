package net.poundex.xmachinery.spring.hx;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.reactive.result.view.Rendering;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

class Hxml extends HypermediaResponseSupport {
	
	private final HxProperties hxProperties;
	
	private final Set<String> triggers = new HashSet<>();

	public Hxml(ServerWebExchange exchange, HxProperties hxProperties) {
		super(exchange);
		this.hxProperties = hxProperties;
	}

	@Override
	public boolean isHypermediaRequest() {
		if(exchange.getRequest().getQueryParams().containsKey("HX"))
			return true;

		return Optional.ofNullable(exchange.getRequest().getHeaders().get(HttpHeaders.ACCEPT)).stream()
				.flatMap(Collection::stream)
				.map(mt -> mt.contains("application/vnd.hyperview_fragment"))
				.findFirst()
				.orElse(false);
	}

	@Override
	public Hx outOfBand() {
		return this;
	}

	@Override
	public Hx decorate(String layoutName) {
		return this;
	}

	@Override
	public Hx status(HttpStatusCode httpStatus) {
		return this;
	}

	@Override
	public <T> T getValue(Supplier<T> web, Supplier<T> mobile) {
		return mobile.get();
	}

	@Override
	public Hx trigger(String event) {
		triggers.add(event);
		return this;
	}

	@Override
	public Flux<Rendering> render(String... views) {
		completeModel();
		return Flux.fromArray(views)
				.map(v -> v + ".xml")
				.map(Rendering::view)
				.map(r -> r.model(model).headers(headers).build());
	}

	private void completeModel() {
		modelAttribute("locale", getLocale());
		modelAttribute("requestUri", "%s?%s".formatted(
				exchange.getRequest().getURI().getPath(), 
				Optional.ofNullable(exchange.getRequest().getURI().getQuery()).orElse("")));
		modelAttribute("requestQuery", Optional.ofNullable(exchange.getRequest().getURI().getQuery()).orElse(""));
		modelAttribute("layout", isHypermediaRequest()
				? Optional.ofNullable(hxProperties.getEmptyLayoutName()).orElse("empty.xml")
				: layoutName.or(() -> Optional.ofNullable(hxProperties.getDefaultLayoutName())).orElse("main.xml"));
		
		if( ! flash.isEmpty()) modelAttribute("flash", flash);
		if( ! triggers.isEmpty()) modelAttribute("triggers", triggers);
		
		exchange.getResponse().getHeaders().set(HttpHeaders.CONTENT_TYPE, 
				isHypermediaRequest()
				? "application/vnd.hyperview_fragment+xml"
				: "application/vnd.hyperview+xml");
	}
}
