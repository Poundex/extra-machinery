package net.poundex.xmachinery.spring.hx;

import org.springframework.http.HttpStatusCode;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.Locale;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public interface Hx {
	Hx modelAttribute(String name, Object value);
	<T> Hx computeModelAttribute(String name, UnaryOperator<T> mapper);
	Hx flash(String style, String message);
	Hx refresh();
	Hx trigger(String event);
	boolean isHypermediaRequest();
	Hx outOfBand();
	Hx decorate(String layoutName);
	Hx status(HttpStatusCode httpStatus);
	<T> T getValue(Supplier<T> web, Supplier<T> mobile);
	default <T> T getValue(T web, T mobile) {
		return getValue(() -> web, () -> mobile);
	}

	default Hx pushUrl(String url) {
		return this;
	}
	
	default <T> Hx all(Collection<T> collection, BiConsumer<T, Hx> fn) {
		collection.forEach(it -> fn.accept(it, (Hx) this));
		return this;
	}

	Locale getLocale();
	Flux<Rendering> render(String... views);

}
