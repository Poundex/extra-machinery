package net.poundex.xmachinery.spring;

import org.reactivestreams.Publisher;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@NoRepositoryBean
public interface ReadOnlyRepository<T, ID> extends Repository<T, ID> {
	/**
	 * Retrieves an entity by its id.
	 *
	 * @param id must not be {@literal null}.
	 * @return {@link Mono} emitting the entity with the given id or {@link Mono#empty()} if none found.
	 * @throws IllegalArgumentException in case the given {@literal id} is {@literal null}.
	 */
	Mono<T> findById(ID id);

	/**
	 * Retrieves an entity by its id supplied by a {@link Publisher}.
	 *
	 * @param id must not be {@literal null}. Uses the first emitted element to perform the find-query.
	 * @return {@link Mono} emitting the entity with the given id or {@link Mono#empty()} if none found.
	 * @throws IllegalArgumentException in case the given {@link Publisher id} is {@literal null}.
	 */
	Mono<T> findById(Publisher<ID> id);

	/**
	 * Returns whether an entity with the given {@literal id} exists.
	 *
	 * @param id must not be {@literal null}.
	 * @return {@link Mono} emitting {@literal true} if an entity with the given id exists, {@literal false} otherwise.
	 * @throws IllegalArgumentException in case the given {@literal id} is {@literal null}.
	 */
	Mono<Boolean> existsById(ID id);

	/**
	 * Returns whether an entity with the given id, supplied by a {@link Publisher}, exists. Uses the first emitted
	 * element to perform the exists-query.
	 *
	 * @param id must not be {@literal null}.
	 * @return {@link Mono} emitting {@literal true} if an entity with the given id exists, {@literal false} otherwise.
	 * @throws IllegalArgumentException in case the given {@link Publisher id} is {@literal null}.
	 */
	Mono<Boolean> existsById(Publisher<ID> id);

	/**
	 * Returns all instances of the type.
	 *
	 * @return {@link Flux} emitting all entities.
	 */
	Flux<T> findAll();

	/**
	 * Returns all instances of the type {@code T} with the given IDs.
	 * <p>
	 * If some or all ids are not found, no entities are returned for these IDs.
	 * <p>
	 * Note that the order of elements in the result is not guaranteed.
	 *
	 * @param ids must not be {@literal null} nor contain any {@literal null} values.
	 * @return {@link Flux} emitting the found entities. The size can be equal or less than the number of given
	 *         {@literal ids}.
	 * @throws IllegalArgumentException in case the given {@link Iterable ids} or one of its items is {@literal null}.
	 */
	Flux<T> findAllById(Iterable<ID> ids);

	/**
	 * Returns all instances of the type {@code T} with the given IDs supplied by a {@link Publisher}.
	 * <p>
	 * If some or all ids are not found, no entities are returned for these IDs.
	 * <p>
	 * Note that the order of elements in the result is not guaranteed.
	 *
	 * @param idStream must not be {@literal null}.
	 * @return {@link Flux} emitting the found entities.
	 * @throws IllegalArgumentException in case the given {@link Publisher idStream} is {@literal null}.
	 */
	Flux<T> findAllById(Publisher<ID> idStream);

	/**
	 * Returns the number of entities available.
	 *
	 * @return {@link Mono} emitting the number of entities.
	 */
	Mono<Long> count();
}
