package net.poundex.autoconverters.ftest.quad

import net.poundex.autoconverters.ftest.FunctionalTestApplication
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = FunctionalTestApplication)
class CopyPropertiesSpec extends Specification {

    @Autowired
    QuadPersistentConverters converters

    void "Copies properties to existing object"() {
        given:
        ObjectId id = new ObjectId()
        Square square = new Square(null, 10, null)
        PersistentSquare persistentSquare = PersistentSquare.builder().id(id).build()
        
        when:
        converters.updateFrom(square, persistentSquare)
        
        then:
        persistentSquare.id == id
        persistentSquare.width == 10
    }
}