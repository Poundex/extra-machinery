package net.poundex.autoconverters.ftest.quad

import net.poundex.autoconverters.ftest.FunctionalTestApplication
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = FunctionalTestApplication)
class PropertyConversionSpec extends Specification {

    @Autowired
    QuadPersistentConverters converters

    void "Converts properties when required"() {
        when:
        Square square = converters.toDomain(PersistentSquare.builder()
                .id(new ObjectId("deadbeefcafebabedeadbeef"))
                .width(8)
                .build())
        
        then:
        square.id == "deadbeefcafebabedeadbeef"
    }
}