package net.poundex.autoconverters.ftest.quad;

import java.util.List;

public record SquareDto(String id, int width, List<Integer> edgeIds) implements QuadDto {
}
