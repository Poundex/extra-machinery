package net.poundex.autoconverters.ftest.quad;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public abstract class Quad {
    private final String id;
    private final int width;
    private final List<String> edgeIds;
}
