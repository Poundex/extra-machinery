package net.poundex.autoconverters.ftest.quad;

import net.poundex.autoconverters.convert.Abstract;
import net.poundex.autoconverters.convert.Converters;
import net.poundex.autoconverters.convert.IgnoreTargetProperty;

@Converters
interface QuadPersistentConverters {
    @Abstract
    Quad toDomain(PersistentQuad persistentQuad);
    Square toDomain(PersistentSquare persistentSquare);
    @IgnoreTargetProperty("ignoredProperty")
    Rectangle toDomain(PersistentRectangle persistentRectangle);
    
    @IgnoreTargetProperty("id")
    void updateFrom(Square square, PersistentSquare persistentSquare);
    void updateFrom(Rectangle rectangle, PersistentRectangle persistentRectangle);
}
