package net.poundex.autoconverters.ftest.quad;

import java.util.List;

public interface QuadDto {
    String id();
    int width();
    List<Integer> edgeIds();
}
