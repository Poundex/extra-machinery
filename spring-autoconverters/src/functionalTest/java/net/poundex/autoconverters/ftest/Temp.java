package net.poundex.autoconverters.ftest;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.autoconverters.ftest.quad.QuadApiConverters;
import net.poundex.autoconverters.ftest.quad.Rectangle;
import org.bson.types.ObjectId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.stream.IntStream;

//@Component
@RequiredArgsConstructor
@Slf4j
public class Temp implements CommandLineRunner {
    
    private final QuadApiConverters apiConverters;

    @Override
    public void run(String... args) throws Exception {
        int iterations = 100_000_000;
        long start = System.nanoTime();
        IntStream.range(0, iterations).forEach(i -> {
            Rectangle r = new Rectangle(new ObjectId().toString(), 10, 5, "ignored", null);
            apiConverters.toDto(r);
        });
        long end = System.nanoTime();
        long millis = Duration.ofNanos(end - start).toMillis();
        log.info(String.format("Performed %s iterations in %sms (%s)",
                iterations,
                millis,
                millis / (double)iterations));
    }
}
