package net.poundex.autoconverters.ftest.quad;

import java.util.List;

public record RectangleDto(String id, int width, int height, List<Integer> edgeIds) implements QuadDto {
}
