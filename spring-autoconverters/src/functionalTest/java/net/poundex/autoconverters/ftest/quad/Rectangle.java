package net.poundex.autoconverters.ftest.quad;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;


@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
public class Rectangle extends Quad {
    private final int height;
    private final String ignoredProperty;
    
    public Rectangle(String id, int width, int height, String ignoredProperty, List<String> edgeIds) {
        super(id, width, edgeIds);
        this.height = height;
        this.ignoredProperty = ignoredProperty;
    }
}
