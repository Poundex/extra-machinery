package net.poundex.autoconverters.ftest.quad;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
abstract class PersistentQuad {
    @Builder.Default
    private ObjectId id = new ObjectId();
    private int width;
}
