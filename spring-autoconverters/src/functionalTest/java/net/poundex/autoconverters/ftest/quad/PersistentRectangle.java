package net.poundex.autoconverters.ftest.quad;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
class PersistentRectangle extends PersistentQuad {
   private int height; 
   
   private String ignoredProperty;
}
