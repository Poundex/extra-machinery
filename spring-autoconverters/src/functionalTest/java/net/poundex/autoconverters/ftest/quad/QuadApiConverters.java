package net.poundex.autoconverters.ftest.quad;

import net.poundex.autoconverters.convert.Abstract;
import net.poundex.autoconverters.convert.Converters;

@Converters
public interface QuadApiConverters {
    @Abstract
    QuadDto toDto(Quad quad);
    SquareDto toDto(Square square);
    RectangleDto toDto(Rectangle rectangle);
}
