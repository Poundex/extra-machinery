package net.poundex.autoconverters.ftest;

import net.poundex.autoconverters.convert.EnableAutoConverters;
import org.bson.types.ObjectId;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;

@SpringBootApplication
@EnableAutoConverters
public class FunctionalTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunctionalTestApplication.class, args);
    }
    
    @Bean
    ConfigurableConversionService conversionService() {
        DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
        
        /* Emulate some existing conversion knowledge a Spring application might have */
        conversionService.addConverter(String.class, ObjectId.class, ObjectId::new);
        conversionService.addConverter(ObjectId.class, String.class, ObjectId::toHexString);
        
        return conversionService;
    }
}
