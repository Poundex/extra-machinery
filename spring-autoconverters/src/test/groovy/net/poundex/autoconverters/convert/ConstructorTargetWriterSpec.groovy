package net.poundex.autoconverters.convert

import net.poundex.xmachinery.test.models.*
import net.poundex.xmachinery.utils.SourceReader
import org.springframework.core.convert.ConversionService
import org.springframework.core.convert.TypeDescriptor
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Function

class ConstructorTargetWriterSpec extends Specification {
    
    ConversionService conversionService = Mock()
    
    void "Writes bean targets"() {
        given:
        @Subject ConstructorTargetWriter writer = 
                new ConstructorTargetWriter(
                        SourceReader.forType(ClassOne), 
                        ClassTwo, 
                        Stub(ConversionService), 
                        Collections.emptySet(), 
                        Collections.emptyMap())

        when:
        def res = writer.write(new ClassOne("some-string"))
        
        then:
        res instanceof ClassTwo
        res.simpleString == "some-string"
    }
    
    void "Writes record targets"() {
        given:
        @Subject ConstructorTargetWriter writer = new ConstructorTargetWriter(
                SourceReader.forType(RecordOne),
                RecordTwo,
                Stub(ConversionService), 
                Collections.emptySet(), 
                Collections.emptyMap())

        when:
        def res = writer.write(new RecordOne("some-string"))

        then:
        res instanceof RecordTwo
        res.simpleString() == "some-string"
    }
    
    void "Converts types when required"() {
        given:
        @Subject ConstructorTargetWriter writer = new ConstructorTargetWriter(
                SourceReader.forType(ConversionSource), 
                ConversionTarget,
                conversionService,
                Collections.emptySet(), 
                Collections.emptyMap())

        when:
        def res = writer.write(new ConversionSource("some-string", 123))

        then:
        1 * conversionService.convert(123, TypeDescriptor.valueOf(Integer), TypeDescriptor.valueOf(String)) >> "123"
        0 * conversionService._
        
        and:
        res instanceof ConversionTarget
        res.string() == "some-string"
        res.integer() instanceof String
        res.integer() == "123"
    }
    
    void "Ignores ignored properties"() {
        given:
        @Subject ConstructorTargetWriter writer = new ConstructorTargetWriter(
                SourceReader.forType(ClassOne),
                ClassTwo, 
                Stub(ConversionService),
                ["simpleString"].toSet(), 
                Collections.emptyMap())

        when:
        def res = writer.write(new ClassOne("some-string"))

        then:
        res instanceof ClassTwo
        res.simpleString == null
    }

    void "Uses property resolvers"() {
        given:
        @Subject ConstructorTargetWriter writer = new ConstructorTargetWriter(
                SourceReader.forType(ConversionSource), 
                ConversionTarget,
                Stub(ConversionService),
                Collections.emptySet(), 
                [integer: { src -> (src.integer * 10).toString() } as Function<Object, Object>])

        when:
        def res = writer.write(new ConversionSource("some-string", 123))

        then:
        res instanceof ConversionTarget
        res.string == "some-string"
        res.integer == "1230"
    }
}
