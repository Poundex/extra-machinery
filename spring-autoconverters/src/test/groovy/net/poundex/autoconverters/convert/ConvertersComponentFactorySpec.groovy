package net.poundex.autoconverters.convert

import javassist.util.proxy.ProxyObject
import net.poundex.autoconverters.convert.ConverterComponentFactory
import net.poundex.autoconverters.convert.ConverterComponentProxyHandler
import net.poundex.autoconverters.convert.SomeConverters
import org.springframework.beans.factory.support.DefaultListableBeanFactory
import org.springframework.beans.factory.support.RootBeanDefinition
import org.springframework.core.convert.support.ConfigurableConversionService
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Supplier

class ConvertersComponentFactorySpec extends Specification {
    
    Supplier<ConfigurableConversionService> conversionServiceProvider = Stub() {
        get() >> Stub(ConfigurableConversionService)
    }
    DefaultListableBeanFactory beanDefinitionRegistry = Mock() {
        getBeanClassLoader() >> { getClass().getClassLoader() }
    }

    @Subject
    ConverterComponentFactory factory = new ConverterComponentFactory(conversionServiceProvider)
    
    void "Registers generated proxy"() {
        when:
        factory.createAndRegisterComponent(SomeConverters.class.name, beanDefinitionRegistry)
        
        then:
        1 * beanDefinitionRegistry.registerBeanDefinition(_, _ as RootBeanDefinition) >> { n, RootBeanDefinition bd ->
            with(bd) { bd2 ->
                bd2.targetType == SomeConverters
                with(bd2.instanceSupplier.get()) { obj ->
                    obj instanceof ProxyObject
                    obj instanceof SomeConverters
                    (obj as ProxyObject).handler instanceof ConverterComponentProxyHandler
                }
            }
        }
    }
}
