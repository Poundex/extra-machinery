package net.poundex.autoconverters.convert

import net.poundex.xmachinery.test.models.ClassSeven
import net.poundex.xmachinery.test.models.MutableClass
import net.poundex.xmachinery.utils.SourceReader
import org.springframework.core.convert.ConversionService
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Function

class SetterTargetWriterSpec extends Specification {
    
    ConversionService conversionService = Stub()
    
    void "Copies properties to target"() {
        given:
        @Subject SetterTargetWriter writer = new SetterTargetWriter(
		        SourceReader.forType(ClassSeven),
		        MutableClass,
		        conversionService, 
                ["number"].toSet(), 
		        [:])


        MutableClass mutableTarget = new MutableClass()
        mutableTarget.setNumber(456)
        
        conversionService.convert(789, _, _) >> "789"
        
        when:
        writer.write(new ClassSeven(123, "some-string", 789), mutableTarget)

        then: "Same types are copied as-is"
        mutableTarget.string == "some-string"

        and: "Properties are converted when required"
        mutableTarget.stringFromNumber == "789"

        and: "Ignored properties are ignored"
        mutableTarget.number == 456
    }
	
	void "Uses property resolvers"() {
		given:
		@Subject SetterTargetWriter writer = new SetterTargetWriter(
				SourceReader.forType(ClassSeven),
				MutableClass,
				conversionService,
				[].toSet(),
				[stringFromNumber: { src -> (src.stringFromNumber * 10).toString() } as Function<Object, Object>])
		
		MutableClass target = new MutableClass()

		when:
		def res = writer.write(
				new ClassSeven(
						123,
						"some-src-string",
						456),
				target)

		then:
		target.number == 123
		target.string == "some-src-string"
		target.stringFromNumber == "4560"
	}
}
