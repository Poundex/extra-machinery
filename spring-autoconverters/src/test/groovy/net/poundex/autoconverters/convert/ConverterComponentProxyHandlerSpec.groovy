package net.poundex.autoconverters.convert

import net.poundex.xmachinery.test.models.*
import org.springframework.beans.factory.support.DefaultListableBeanFactory
import org.springframework.core.convert.support.ConfigurableConversionService
import spock.lang.Specification

import java.util.function.Supplier

class ConverterComponentProxyHandlerSpec extends Specification {

    ConfigurableConversionService conversionService = Mock(ConfigurableConversionService)
    Supplier<ConfigurableConversionService> conversionServiceProvider = Stub() {
        get() >> conversionService
    }

    DefaultListableBeanFactory beanDefinitionRegistry = Stub() {
        getBeanClassLoader() >> { getClass().getClassLoader() }
    }

    ConverterComponentFactory factory = new ConverterComponentFactory(conversionServiceProvider)

    void "Registers converters for discovered definitions"() {
        when:
        factory.createAndRegisterComponent(SomeConverters.class.name, beanDefinitionRegistry).get()
        
        then: "Simple converters are added"
        1 * conversionService.addConverter(ClassOne, ClassTwo, _)
        1 * conversionService.addConverter(ClassThreeOne, ClassFive, _)
        1 * conversionService.addConverter(ClassThreeTwo, ClassSix, _)
        
        and: "All converters that match the Abstract are added"
        1 * conversionService.addConverter(ClassThreeOne, ClassFiveOrSix, _)
        1 * conversionService.addConverter(ClassThreeTwo, ClassFiveOrSix, _)
        
        and: "The abstract converter itself is added"
        1 * conversionService.addConverter(AbstractClassThree, ClassFiveOrSix, _)
        
        and: 
        0 * conversionService.addConverter(*_)
    }

    void "Performs correct conversions"() {
        given:
        SomeConverters someConverters = factory.createAndRegisterComponent(SomeConverters.class.name, beanDefinitionRegistry).get()
        ClassOne one = new ClassOne("some-string")
        ClassThreeOne threeOne = new ClassThreeOne("some-string")
        ClassThreeTwo threeTwo = new ClassThreeTwo("some-string")
        
        when:
        someConverters.toClassTwo(one)
        someConverters.toFive(threeOne)
        someConverters.toSix(threeTwo)
        
        then:
        1 * conversionService.convert(one, ClassTwo)
        1 * conversionService.convert(threeOne, ClassFive)
        1 * conversionService.convert(threeTwo, ClassSix)
        
        when:
        someConverters.toFiveOrSix(threeOne)
        someConverters.toFiveOrSix(threeTwo)
        
        then:
        1 * conversionService.convert(threeOne, ClassFiveOrSix)
        1 * conversionService.convert(threeTwo, ClassFiveOrSix)
    }
    
}