package net.poundex.autoconverters.convert;

import net.poundex.xmachinery.test.models.AbstractClassThree;
import net.poundex.xmachinery.test.models.ClassFive;
import net.poundex.xmachinery.test.models.ClassFiveOrSix;
import net.poundex.xmachinery.test.models.ClassOne;
import net.poundex.xmachinery.test.models.ClassSeven;
import net.poundex.xmachinery.test.models.ClassSix;
import net.poundex.xmachinery.test.models.ClassThreeOne;
import net.poundex.xmachinery.test.models.ClassThreeTwo;
import net.poundex.xmachinery.test.models.ClassTwo;
import net.poundex.xmachinery.test.models.MutableClass;

@Converters
public interface SomeConverters {
    ClassTwo toClassTwo(ClassOne classOne);
    
    @Abstract
    ClassFiveOrSix toFiveOrSix(AbstractClassThree abstractClassThree);
    ClassFive toFive(ClassThreeOne classThreeOne);
    ClassSix toSix(ClassThreeTwo classThreeTwo);

    @IgnoreTargetProperty("id")
    void copyToMutable(ClassSeven classSeven, MutableClass mutableClass);
}
