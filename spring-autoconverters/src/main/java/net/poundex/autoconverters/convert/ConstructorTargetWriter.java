package net.poundex.autoconverters.convert;

import lombok.extern.slf4j.Slf4j;
import net.poundex.xmachinery.utils.ReflectionUtil;
import net.poundex.xmachinery.utils.SourceReader;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

@Slf4j
class ConstructorTargetWriter extends TargetWriter {

    private final Constructor<?> targetConstructor;
    private final Map<String, Function<Object, Object>> propertyResolvers;

    public ConstructorTargetWriter(SourceReader sourceReader, Class<?> targetType, ConversionService conversionService, Set<String> ignoredProperties, Map<String, Function<Object, Object>> propertyResolvers) {
        super(sourceReader, targetType, conversionService, ignoredProperties);
        this.targetConstructor = findConstructor(targetType);
        this.propertyResolvers = propertyResolvers;
    }

    Object write(Object source) {
        Object[] ctorArgsInOrder = Arrays.stream(targetConstructor.getParameters())
                .map(param -> getTargetValue(source, param))
                .toArray();
        return ReflectionUtil.invokeConstructor(targetConstructor, ctorArgsInOrder);
    }

    private Object getTargetValue(Object source, Parameter param) {
        if(ignoredProperties.contains(param.getName()))
            return getDefaultValue(param.getType());
        
        if(propertyResolvers.containsKey(param.getName()))
            return propertyResolvers.get(param.getName()).apply(source);
        
        return sourceReader.readProperty(source, param.getName())
                .map(sourceValue -> toWantedParamType(sourceValue,
                        new TypeDescriptor(MethodParameter.forParameter(param))))
                .orElseGet(() -> getDefaultValue(param.getType()));
    }

    private static Constructor<?> findConstructor(Class<?> targetType) {
        List<Constructor<?>> candidateTargetCtors = Arrays.stream(targetType.getConstructors())
                .filter(it -> it.getParameterCount() != 0)
                .toList();

        if (candidateTargetCtors.isEmpty())
            throw new IllegalStateException(
                    String.format("Did not find a non-default constructor in %s", targetType.getName()));

        if (candidateTargetCtors.size() != 1)
            throw new IllegalStateException(
                    String.format("Found more than one non-default constructor in %s", targetType.getName()));

        return candidateTargetCtors.get(0);
    }
}