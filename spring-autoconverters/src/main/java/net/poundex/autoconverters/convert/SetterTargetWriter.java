package net.poundex.autoconverters.convert;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import net.poundex.xmachinery.utils.ReflectionUtil;
import net.poundex.xmachinery.utils.SourceReader;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.util.ReflectionUtils;

import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
class SetterTargetWriter extends TargetWriter {

    private record Setter(String propertyName, TypeDescriptor propertyType, Method method) {}
    
    private final Map<String, Setter> setters;
    private final Map<String, Function<Object, Object>> propertyResolvers;

    public SetterTargetWriter(SourceReader sourceReader, Class<?> targetType, ConversionService conversionService, Set<String> ignoredProperties, Map<String, Function<Object, Object>> propertyResolvers) {
        super(sourceReader, targetType, conversionService, ignoredProperties);
        this.setters = Try.of(() -> Introspector.getBeanInfo(targetType))
                .toJavaStream()
                .flatMap(bi -> Arrays.stream(bi.getPropertyDescriptors()))
                .filter(prop -> ! ignoredProperties.contains(prop.getName()))
//                .filter(prop -> ! propertyResolvers.containsKey(prop.getName()))
                .filter(prop -> prop.getWriteMethod() != null)
                .peek(prop -> ReflectionUtils.makeAccessible(prop.getWriteMethod()))
                .collect(Collectors.toMap(FeatureDescriptor::getName, this::toSetter));
        this.propertyResolvers = propertyResolvers;
    }

    void write(Object source, Object target) {
        setters.forEach((propName, setter) -> {
            if(propertyResolvers.containsKey(propName))
                ReflectionUtil.invokeMethod(
                        setter.method(), target, propertyResolvers.get(propName).apply(source));
            else 
    	        sourceReader.readProperty(source, propName)
			        .ifPresent(v -> {
				        invokeSetter(target, setter, v);
			        });
        });
    }

    private void invokeSetter(Object target, Setter setter, SourceReader.TypedPropertyValue v) {
        Object value = toWantedParamType(v, setter.propertyType());
        try {
            ReflectionUtil.invokeMethod(setter.method(), target, value);
        }
        catch (IllegalArgumentException iaex) {
            throw new IllegalArgumentException(String.format(
                    "When invoking %s(%s) with %s %s",
                    setter.method().getName(),
                    Arrays.stream(setter.method().getParameterTypes())
                            .map(Class::getSimpleName)
                            .collect(Collectors.joining(", ")),
                    value.getClass().getSimpleName(),
                    value), iaex);
        }
    }

    private Setter toSetter(PropertyDescriptor property) {
        return new Setter(property.getName(), new TypeDescriptor(
                ResolvableType.forMethodParameter(
                        property.getWriteMethod(), 0),
                null,
                null),
                property.getWriteMethod());
    }

}