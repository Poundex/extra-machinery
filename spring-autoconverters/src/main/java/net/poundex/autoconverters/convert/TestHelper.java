package net.poundex.autoconverters.convert;

import io.vavr.Lazy;
import net.poundex.autoconverters.convert.ConverterComponentFactory;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;

public class TestHelper {
	
	private static final Lazy<ConfigurableConversionService> conversionService = 
			Lazy.of(DefaultFormattingConversionService::new);
	
	public static <T> T createComponent(Class<T> klass) {
		return new ConverterComponentFactory(conversionService).createComponent(klass).get();
	}
}
