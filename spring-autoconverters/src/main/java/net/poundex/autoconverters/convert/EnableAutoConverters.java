package net.poundex.autoconverters.convert;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(ConverterComponentRegistrar.class)
public @interface EnableAutoConverters {
   String basePackage() default ""; 
}