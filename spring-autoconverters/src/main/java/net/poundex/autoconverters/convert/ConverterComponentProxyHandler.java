package net.poundex.autoconverters.convert;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import javassist.util.proxy.MethodHandler;
import lombok.extern.slf4j.Slf4j;
import net.poundex.autoconverters.TypePair;
import net.poundex.xmachinery.utils.ReflectionUtil;
import net.poundex.xmachinery.utils.SourceReader;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.ConfigurableConversionService;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
class ConverterComponentProxyHandler implements MethodHandler {
    
    private final ConfigurableConversionService conversionService;

    private final Map<TypePair, Function<Object[], Object>> convertingHandlers = new HashMap<>();
    private final Map<Method, Function<Object[], Object>> handlers = new HashMap<>();
    private final Map<TypePair, Map<String, Function<Object, Object>>> propertyResolvers = new HashMap<>();
    private final Object proxy;

    private final LoadingCache<Class<?>, SourceReader> sourceReaders =
            Caffeine.newBuilder().build(SourceReader::forType);
    private Map<TypePair, Set<String>> abstractIgnoreProperties = new HashMap<>();

    public ConverterComponentProxyHandler(ConfigurableConversionService conversionService, Method[] mapperMethods, Object proxy) {
        this.conversionService = conversionService;
        this.proxy = proxy;
        createAllHandlers(mapperMethods);
    }

    @Override
    public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) {
        if(thisMethod.isDefault())
            return ReflectionUtil.invokeMethod(proceed, self, args);
        
        return handlers.get(thisMethod).apply(args);
    }

    private void createAllHandlers(Method[] mapperMethods) {
        record MethodHandlerFactory(Method method, Supplier<Function<Object[], Object>> handlerFactory) { }
        List<Runnable> doLater = new ArrayList<>();
        List<MethodHandlerFactory> doLast = new ArrayList<>();
        Arrays.stream(mapperMethods).forEach((m -> {
            if (m.isAnnotationPresent(Abstract.class)) {
                abstractIgnoreProperties.put(TypePair.fromConvertMethod(m),
                        Arrays.stream(m.getAnnotationsByType(IgnoreTargetProperty.class))
                                .map(IgnoreTargetProperty::value)
                                .collect(Collectors.toSet()));

                doLast.add(new MethodHandlerFactory(m, () -> createAbstractHandler(m)));
            }
            else if (m.getReturnType() == void.class && m.getParameterCount() == 2) {
                doLast.add(new MethodHandlerFactory(m, () -> createCopyHandler(m)));
            }
            else if(m.isDefault()) {
                PropertyResolver propResolver = m.getAnnotation(PropertyResolver.class);
                if(propResolver != null) {
                    TypePair sourceTarget = TypePair.fromClasses(m.getParameterTypes()[0], propResolver.value());
                    if ( ! propertyResolvers.containsKey(sourceTarget))
                        propertyResolvers.put(sourceTarget, new HashMap<>());
                    
                    propertyResolvers.get(sourceTarget).put(m.getName(),
                            source -> ReflectionUtil.invokeMethod(m, proxy, source));
                }
                else {
                    conversionService.addConverter(
                            (Class<Object>) m.getParameterTypes()[0],
                            (Class<Object>) m.getReturnType(),
                            src -> ReflectionUtil.invokeMethod(m, proxy, src));
                }
            }
            else {
                doLater.add(() -> {
                    Function<Object[], Object> handler = createConverterHandler(m);
                    convertingHandlers.put(TypePair.fromConvertMethod(m), handler);
                    handlers.put(m, handler);
                });
            }
        }));
        doLater.forEach(Runnable::run);
        doLast.forEach(s -> handlers.put(s.method(), s.handlerFactory().get()));
    }

    private Function<Object[], Object> createCopyHandler(Method m) {
        TypePair typePair = TypePair.fromCopyMethod(m);
        
        SetterTargetWriter targetWriter = new SetterTargetWriter(
                sourceReaders.get(m.getParameterTypes()[0]), 
                m.getParameterTypes()[1], 
                conversionService, 
                getIgnoreTargetProperties(m), 
                propertyResolvers.getOrDefault(typePair, Collections.emptyMap()));
        return args -> {
            targetWriter.write(args[0], args[1]);
            return null;
        };
    }

    @SuppressWarnings("unchecked")
    private Function<Object[], Object> createConverterHandler(Method method) {
        TypePair typePair = TypePair.fromConvertMethod(method);

        log.atDebug().setMessage("Creating simple converter")
                .addKeyValue("sourceType", typePair.source())
                .addKeyValue("targetType", typePair.target())
                .log();

        Set<String> ignore = Stream.concat(
                abstractIgnoreProperties.entrySet().stream()
                        .filter(kv -> typePair.isAssignableTo(kv.getKey()))
                        .flatMap(kv -> kv.getValue().stream()), 
                        getIgnoreTargetProperties(method).stream())
                .collect(Collectors.toSet());

        conversionService.addConverter(
                (Class<Object>) typePair.source().getType(),
                (Class<Object>) typePair.target().getType(),
                (Converter<Object, Object>) createConverter(typePair, ignore));
        
        return args -> conversionService.convert(args[0], method.getReturnType());
    }

    private Converter<?, ?> createConverter(TypePair typePair, Set<String> ignoreTargetProperties) {
        
        ConstructorTargetWriter targetWriter = new ConstructorTargetWriter(
                sourceReaders.get(typePair.source().getType()),
                typePair.target().getType(),
                conversionService,
                ignoreTargetProperties, 
                getPropertyResolvers(typePair));

        return targetWriter::write;
    }

    private Map<String, Function<Object, Object>> getPropertyResolvers(TypePair typePair) {
        return propertyResolvers.entrySet().stream()
                .filter(kv -> typePair.isAssignableTo(kv.getKey()))
                .flatMap(kv -> kv.getValue().entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @SuppressWarnings("unchecked")
    private Function<Object[], Object> createAbstractHandler(Method method) {
        TypePair abstractTypes = TypePair.fromConvertMethod(method);
        
        log.atDebug().setMessage("Creating abstract converter")
                .addKeyValue("sourceType", abstractTypes.source())
                .addKeyValue("targetType", abstractTypes.target())
                .log();
        
        convertingHandlers.keySet().stream()
                .filter(st -> st.source().isAssignableTo(abstractTypes.source()) 
                        && st.target().isAssignableTo(abstractTypes.target()))
                .peek(st -> log.atDebug().setMessage("Creating abstract target")
                        .addKeyValue("sourceType", st.source())
                        .addKeyValue("targetType", abstractTypes.target())
                        .log())
                .forEach(st -> {
	                conversionService.addConverter(
			                (Class<Object>) st.source().getType(),
			                (Class<Object>) abstractTypes.target().getType(),
			                s -> convertingHandlers.get(st).apply(new Object[]{s}));
                });

        Function<Object[], Object> objectFunction = args -> conversionService.convert(args[0], abstractTypes.target().getType());
        conversionService.addConverter(
                (Class<Object>) abstractTypes.source().getType(),
                (Class<Object>) abstractTypes.target().getType(), 
                cp -> objectFunction.apply(new Object[]{cp}));
        return objectFunction;
    }
    
    private Set<String> getIgnoreTargetProperties(Method method) {
        return Arrays.stream(method.getAnnotationsByType(IgnoreTargetProperty.class))
                .map(IgnoreTargetProperty::value)
                .collect(Collectors.toSet());
    }
}
