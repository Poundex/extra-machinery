package net.poundex.autoconverters.convert;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.RequiredArgsConstructor;
import net.poundex.autoconverters.TypePair;
import net.poundex.xmachinery.utils.SourceReader;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RequiredArgsConstructor
abstract class TargetWriter {
    
    protected static final LoadingCache<TypePair, Boolean> assignabilityCache = Caffeine.newBuilder()
            .build(types -> types.source().isAssignableTo(types.target()));
    
    protected final SourceReader sourceReader;
    protected final Class<?> targetType;
    protected final ConversionService conversionService;
    protected final Set<String> ignoredProperties;

    protected static Object getDefaultValue(Class<?> type) {
        if (type.isPrimitive())
            return Array.get(Array.newInstance(type, 1), 0);
        
        if(List.class.isAssignableFrom(type))
            return Collections.emptyList();
        if(Set.class.isAssignableFrom(type))
            return Collections.emptySet();
        if(Map.class.isAssignableFrom(type))
            return Collections.emptyMap();
        
        return null;
    }

    protected Object toWantedParamType(SourceReader.TypedPropertyValue sourceValue, TypeDescriptor targetType) {
        if(sourceValue.value() == null)
            return null;
        
//        if(targetType.getType().isAssignableFrom(sourceValue.type().getType()) || 
//                sourceValue.type().isAssignableTo(targetType))
//            return sourceValue.value();
        
        if(assignabilityCache.get(new TypePair(sourceValue.type(), targetType)))
            return sourceValue.value();
        
        return conversionService.convert(sourceValue.value(), sourceValue.type(), targetType);
    }
}
