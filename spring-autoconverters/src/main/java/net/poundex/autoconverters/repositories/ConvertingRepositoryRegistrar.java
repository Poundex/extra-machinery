package net.poundex.autoconverters.repositories;

import lombok.extern.slf4j.Slf4j;
import net.poundex.xmachinery.utils.ReflectionUtil;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.StandardAnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.StringUtils;

import java.util.function.Supplier;

@Slf4j
class ConvertingRepositoryRegistrar implements ImportBeanDefinitionRegistrar, BeanFactoryAware {

    private Supplier<ConfigurableConversionService> conversionService;
    private RepositoryAdapterFactory repositoryAdapterFactory;
	private ClassLoader beanClassLoader;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.conversionService = beanFactory.getBeanProvider(ConfigurableConversionService.class)::getObject;
		this.beanClassLoader = ((ConfigurableBeanFactory) beanFactory).getBeanClassLoader();
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

       String basePackage;
       String explicitBp = importingClassMetadata
                .getAnnotationAttributes(EnableConvertingRepositories.class.getName())
                .get("basePackage")
                .toString();
       
        if(StringUtils.hasText(explicitBp))
            basePackage = explicitBp;
        else if(importingClassMetadata instanceof StandardAnnotationMetadata amd)
           basePackage = amd.getIntrospectedClass()
                .getPackage()
                .getName();
        else
            basePackage = ReflectionUtil.classForName(importingClassMetadata.getClassName(), ((ConfigurableBeanFactory) registry).getBeanClassLoader()).getPackage().getName();

	    this.repositoryAdapterFactory = new RepositoryAdapterFactory(conversionService, beanClassLoader);

        ClassPathScanningCandidateComponentProvider p = new ClassPathScanningCandidateComponentProvider(false) {
            @Override
            protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
                return super.isCandidateComponent(beanDefinition)
                        || beanDefinition.getMetadata().isInterface();
            }
        };
        p.addIncludeFilter(new AnnotationTypeFilter(DaoFor.class));
        p.findCandidateComponents(basePackage).stream()
                .peek(def -> log.atInfo()
                        .setMessage("Registering repository adapter")
                        .addKeyValue("class", def.getBeanClassName())
                        .log())
                .forEach(def -> repositoryAdapterFactory
                        .registerAdapter(def.getBeanClassName(), registry));
    }
}
