package net.poundex.autoconverters.repositories;

import io.vavr.collection.Stream;
import jakarta.validation.Validator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodCall;
import net.bytebuddy.implementation.bytecode.assign.Assigner;
import net.bytebuddy.matcher.ElementMatchers;
import net.poundex.xmachinery.utils.ReflectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.data.repository.Repository;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
class RepositoryAdapterFactory {

    private final Supplier<ConfigurableConversionService> conversionServiceProvider;
    private final ClassLoader classLoader;

    @SuppressWarnings("unchecked")
    <D, Dbo, Dao extends Repository<Dbo, ?>, Repo extends Repository<D, ?>>
    void registerAdapter(String daoClassName, BeanDefinitionRegistry registry) {
    
        Class<Dao> daoClass = (Class<Dao>) ReflectionUtil.classForName(daoClassName, ((ConfigurableBeanFactory) registry).getBeanClassLoader());
        Class<Repo> repositoryImplClass = createRepositoryImplClass(daoClass);
        registry.registerBeanDefinition(daoClass.getSimpleName(),
                BeanDefinitionBuilder.rootBeanDefinition(repositoryImplClass).getBeanDefinition());
    }

    @SuppressWarnings("unchecked")
    private <D, Dbo, Dao extends Repository<Dbo, ?>, Repo extends Repository<D, ?>>
    Class<Repo> createRepositoryImplClass(Class<Dao> daoClass) {

        Class<Repo> repositoryClass = (Class<Repo>) daoClass.getAnnotation(DaoFor.class).value();
        ResolvableType repositoryType = ResolvableType.forType(repositoryClass);
        ResolvableType daoType = ResolvableType.forType(daoClass);
        ResolvableType springRepositoryType = ReflectionUtil.findGenericSuperInterface(repositoryType, Repository.class);
        ResolvableType domainType = springRepositoryType.getGeneric(0);
        ResolvableType dboType = ReflectionUtil.findGenericSuperInterface(daoType, Repository.class).getGeneric(0);

        try {
            TypeDescription.Generic repoType = TypeDescription.Generic.Builder.parameterizedType(AbstractConvertingRepository.class,
                            domainType.getType(), dboType.getType(), daoClass, repositoryType.getType())
                    .build();
            return (Class<Repo>) Stream.of(repositoryType.getRawClass().getMethods())
                    .foldLeft((DynamicType.Builder<Repo>) new ByteBuddy()
                                    .subclass(repoType)
                                    .implement(toBbType(repositoryType))
                                    .name(daoClass.getPackageName() + "." +  repositoryClass.getSimpleName() + "$Impl")
                                    .defineField("dao", daoClass)
                                    .constructor(ElementMatchers.isConstructor().and(ElementMatchers.not(ElementMatchers.isDefaultConstructor())))
                                    .intercept(MethodCall.invoke(AbstractConvertingRepository.class.getConstructor(Repository.class, ConversionService.class, Validator.class)).withAllArguments())
                                    .annotateMethod(AnnotationDescription.Builder.ofType(Autowired.class).build()),
                            (builder, repositoryMethod) -> {
                                ResolvableType repositoryMethodReturnType = ResolvableType.forMethodReturnType(repositoryMethod, repositoryType.getRawClass());
                                log.atDebug()
                                        .addArgument(() -> toString(repositoryMethodReturnType))
                                        .addArgument(repositoryMethod.getName())
                                        .addArgument(() -> Arrays.stream(repositoryMethod.getParameters())
                                                .map(p -> ResolvableType.forMethodParameter(MethodParameter.forParameter(p), repositoryType))
                                                .map(this::toString)
                                                .collect(Collectors.joining(", ")))
                                        .log("Method! {} {}({})");

                                TypeDefinition bbRepoMethodReturnType = toBbType(repositoryMethodReturnType);
                                List<ResolvableType> realRepoMethodParams = Arrays.stream(repositoryMethod.getParameters())
                                        .map(p -> ResolvableType.forMethodParameter(MethodParameter.forParameter(p), repositoryType))
                                        .toList();
                                List<TypeDefinition> bbRepoMethodParams = realRepoMethodParams.stream()
                                        .map(RepositoryAdapterFactory::toBbType)
                                        .toList();
                                Class<?>[] rawDaoParams = Arrays.stream(repositoryMethod.getParameters())
                                        .map(Parameter::getType)
                                        .toArray(Class[]::new);

                                Method targetDaoMethod = ReflectionUtils.findMethod(daoType.getRawClass(), repositoryMethod.getName(), rawDaoParams);
                                if (targetDaoMethod == null) {
                                    log.info("No dao method for {}({})", repositoryMethod.getName(), rawDaoParams);
                                    return builder;
                                }
                                ReflectionUtils.makeAccessible(targetDaoMethod);

                                List<ResolvableType> realTargetDaoMethodParams = Arrays.stream(repositoryMethod.getParameters())
                                        .map(p -> ResolvableType.forMethodParameter(MethodParameter.forParameter(p), daoType))
                                        .toList();

                                ResolvableType realTargetDaoMethodReturnType =
                                        ResolvableType.forMethodReturnType(targetDaoMethod, daoType.getRawClass());

                                log.debug("Found dao method {} {}({}))",
                                        toString(realTargetDaoMethodReturnType),
                                        targetDaoMethod.getName(),
                                        realTargetDaoMethodParams.stream().map(this::toString).toList());

                                Function<Object, Object> returnTypeAdapter = createAdapter(realTargetDaoMethodReturnType, repositoryMethodReturnType);
                                List<Function<Object, Object>> paramsAdapters = Stream.ofAll(realRepoMethodParams)
                                        .zip(realTargetDaoMethodParams)
                                        .map(st -> createAdapter(st._1(), st._2()))
                                        .toJavaList();

                                @SuppressWarnings("Convert2Lambda") 
                                BiFunction<Object, Object[], Object> executable = new BiFunction<>() {
	                                @Override
	                                public Object apply(Object obj, Object[] args) {
		                                try {
			                                return targetDaoMethod.invoke(obj, args);
		                                } catch (IllegalAccessException | InvocationTargetException e) {
			                                throw new RuntimeException(e);
		                                }
	                                }
                                };
                                log.debug("Defining method {} {}({})", bbRepoMethodReturnType, repositoryMethod.getName(), bbRepoMethodParams);

                                DynamicType.Builder.MethodDefinition.TypeVariableDefinition<Repo> mb = builder
                                        .defineMethod(repositoryMethod.getName(), bbRepoMethodReturnType, repositoryMethod.getModifiers() ^ Modifier.ABSTRACT)
                                        .withParameters(bbRepoMethodParams);

                                DynamicType.Builder.MethodDefinition.ImplementationDefinition<Repo> mb3 = Stream.of(repositoryMethod.getTypeParameters())
                                        .foldLeft(mb, (mb2, tv) -> {
                                            if (ResolvableType.forType(tv).resolve() == null)
                                                return mb2.typeVariable(tv.getName());
                                            return mb2;
                                        });

                                return mb3.intercept(MethodCall.invoke(ElementMatchers.named("adaptArguments"))
                                        .withArgumentArray()
                                        .with(executable)
                                        .with(returnTypeAdapter)
                                        .with(paramsAdapters)
                                        .withAssigner(Assigner.DEFAULT, Assigner.Typing.DYNAMIC));
                            })
                    .make()
                    .load(classLoader)
                    .getLoaded();
        }
        catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private Function<Object, Object> createAdapter(ResolvableType sourceType, ResolvableType targetType) {
        if(targetType.isAssignableFrom(sourceType))
            //noinspection Convert2Lambda
            return new Function<>() {
	            @Override
	            public Object apply(Object o) {
		            return o;
	            }
            };
        
        TypeDescriptor srcTd;
        TypeDescriptor tgtTd;
        
        if( ! sourceType.hasGenerics())
            srcTd = TypeDescriptor.valueOf(sourceType.resolve());
        else 
            srcTd = new TypeDescriptor(ResolvableType.forClassWithGenerics(
                    sourceType.resolve(), sourceType.resolveGenerics()), null, null);


        if( ! targetType.hasGenerics())
            tgtTd = TypeDescriptor.valueOf(targetType.resolve());
        else
            tgtTd = new TypeDescriptor(ResolvableType.forClassWithGenerics(
                    targetType.resolve(), targetType.resolveGenerics()), null, null);
        
        //noinspection Convert2Lambda
        return new Function<>() {
	        @Override
	        public Object apply(Object in) {
		        return conversionServiceProvider.get().convert(in, 
				        srcTd, 
				        tgtTd);
	        }
        };
    }

    private static TypeDefinition toBbType(ResolvableType resolvableType) {
        
        TypeDefinition r;
        
        if(resolvableType.resolve() == null && ! (resolvableType.getType() instanceof WildcardType))
            r = TypeDescription.Generic.Builder.of(resolvableType.getType()).build();
        
        else if (resolvableType.resolve() == null && resolvableType.getType() instanceof WildcardType)
            r = TypeDescription.Generic.Builder.unboundWildcard();

        else if( ! resolvableType.hasGenerics())
            r = TypeDescription.ForLoadedType.of(resolvableType.resolve());
        
        else
            r = TypeDescription.Generic.Builder
                .parameterizedType(TypeDescription.ForLoadedType.of(resolvableType.resolve()),
                        Arrays.stream(resolvableType.getGenerics())
                                .map(RepositoryAdapterFactory::toBbType)
                                .toList())
                .build();
        
        log.debug("toBBType({}) ==> {}", resolvableType, r);
        return r;
    }

    private String toString(ResolvableType p) {
        if(p.resolve() == null)
            return "?";
            
        String outer = p.resolve().getSimpleName();
        if(p.hasGenerics())
            return String.format("%s<%s>", outer, Arrays.stream(p.getGenerics())
                    .map(this::toString)
                    .collect(Collectors.joining(", ")));
        
        return outer;
    }

}
