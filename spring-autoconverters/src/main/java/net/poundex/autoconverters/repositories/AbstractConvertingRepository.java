package net.poundex.autoconverters.repositories;

import io.vavr.collection.Stream;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import jakarta.validation.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

@RequiredArgsConstructor
public class AbstractConvertingRepository<
		D,
		Dbo,
		Dao extends Repository<Dbo, ?>,
		Repo extends Repository<D, ?>> {
	
	protected final Dao dao;
	protected final ConversionService conversionService;
	protected final Validator validator;
	
	@SuppressWarnings("unchecked")
	public <T> T adaptArguments(
			Object[] invokeArgs,
			BiFunction<Object, Object[], Object> targetExecutable, 
			Function<Object, Object> returnTypeAdapter, 
			List<Function<Object, Object>> paramsAdapters) {
	
		return (T) returnTypeAdapter.apply(targetExecutable.apply(dao,
				Stream.of(invokeArgs)
						.peek(this::validate)
						.zip(paramsAdapters)
						.map(argAdapt -> argAdapt._2().apply(argAdapt._1()))
						.toJavaArray(Object[]::new)));
	}

	private void validate(Object object) {
		Valid valid = AnnotationUtils.getAnnotation(object.getClass(), Valid.class);
		if(valid == null) return;

		Set<ConstraintViolation<Object>> errors = validator.validate(object);
		if( ! errors.isEmpty())
			throw new ConstraintViolationException(errors);
	}
}
