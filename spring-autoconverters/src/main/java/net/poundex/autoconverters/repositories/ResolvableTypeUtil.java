package net.poundex.autoconverters.repositories;

import org.springframework.core.ResolvableType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ResolvableTypeUtil {
	
	private static final Method resolveType;
	
	static {
		try {
			resolveType = ResolvableType.class.getDeclaredMethod("resolveType");
			resolveType.setAccessible(true);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static ResolvableType resolveType(ResolvableType resolvableType) {
		try {
			if( ! resolvableType.hasGenerics())
				return resolvableType;
			return (ResolvableType) resolveType.invoke(resolvableType);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
}
