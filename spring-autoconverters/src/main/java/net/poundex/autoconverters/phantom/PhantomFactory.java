package net.poundex.autoconverters.phantom;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;

import java.util.Set;

public class PhantomFactory {
	
	private static final Set<String> ID_ACCESSOR_METHODS = Set.of("id", "getId");
	
	public static <T> T createPhantomFor(Class<T> klass, Object id) {
		return ProxyFactory.getProxy(klass, (MethodInterceptor) invocation -> {
			if (ID_ACCESSOR_METHODS.contains(invocation.getMethod().getName())
					&& invocation.getMethod().getParameterCount() == 0)
				return id;
			
			return null;
		});
	}
}
