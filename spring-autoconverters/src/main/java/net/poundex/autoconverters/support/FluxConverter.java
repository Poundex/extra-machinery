package net.poundex.autoconverters.support;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Collections;
import java.util.Set;

@RequiredArgsConstructor
@Component
class FluxConverter implements ConditionalGenericConverter {

	private final ConversionService conversionService;
	
	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Collections.singleton(new ConvertiblePair(Flux.class, Flux.class));
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return ConversionUtils.canConvertElements(
				sourceType.getElementTypeDescriptor(), 
				targetType.getElementTypeDescriptor(), 
				this.conversionService);
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		if (source == null)
			return null;
		if(sourceType.getResolvableType().getGeneric(0).equals(targetType.getResolvableType().getGeneric(0)))
			return source;

		Flux<?> sourceFlux = (Flux<?>) source;
		TypeDescriptor targetElementType = 
				TypeDescriptor.valueOf(targetType.getResolvableType().getGeneric(0).resolve());
		return sourceFlux.map(elem -> this.conversionService.convert(
				elem,
				sourceType.elementTypeDescriptor(elem),
				targetElementType));
	}
}
