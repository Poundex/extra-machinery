package net.poundex.autoconverters.support;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Component
class IterableConverter implements ConditionalGenericConverter {

	private final ConversionService conversionService;
	
	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Collections.singleton(new ConvertiblePair(Iterable.class, Iterable.class));
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return ConversionUtils.canConvertElements(
				sourceType.getElementTypeDescriptor(), 
				targetType.getElementTypeDescriptor(), 
				this.conversionService);
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		if (source == null)
			return null;
		if (sourceType.getResolvableType().getGeneric(0).equals(targetType.getResolvableType().getGeneric(0)))
			return source;

		Iterable<?> sourceIterable = (Iterable<?>) source;

		TypeDescriptor targetElementType =
				TypeDescriptor.valueOf(targetType.getResolvableType().getGeneric(0).resolve());
		return StreamSupport.stream(sourceIterable.spliterator(), false)
				.map(elem -> this.conversionService.convert(
						elem,
						sourceType.elementTypeDescriptor(elem),
						targetElementType))
				.toList();
	}
}
