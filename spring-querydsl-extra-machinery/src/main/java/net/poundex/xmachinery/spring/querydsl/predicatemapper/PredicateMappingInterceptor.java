package net.poundex.xmachinery.spring.querydsl.predicatemapper;

import com.querydsl.core.support.ReplaceVisitor;
import com.querydsl.core.types.Constant;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Operation;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import net.poundex.xmachinery.utils.ReflectionUtil;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.core.annotation.AnnotationUtils;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
class PredicateMappingInterceptor implements MethodInterceptor {
	
	private final Object target;
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		ProxyMethodInvocation pmi = (ProxyMethodInvocation) invocation;
		pmi.setArguments(Arrays.stream(pmi.getArguments())
				.map(arg -> {
					if(arg instanceof Predicate p) {
						return Optional.<Object>ofNullable(p.accept(new ReplaceVisitor<>() {
							@Override
							public Expression<?> visit(Operation<?> expr, Object context) {
								if(expr.getArgs().get(0) instanceof Path<?> exprPath &&
										expr.getArg(1) instanceof Constant<?> exprConstant) {
									
									return (Predicate) Arrays.stream(target.getClass().getMethods())
											.filter(m -> m.getName().equals(exprPath.getMetadata().getName()))
											.filter(m -> AnnotationUtils.findAnnotation(m, PredicateMapper.class) != null)
											.findFirst()
											.map(m -> ReflectionUtil.invokeMethod(m, target, exprConstant.getConstant()))
											.orElseGet(() -> super.visit(expr, context));
											
								}
								return super.visit(expr, context);
							}
						}, null)).orElse(p);
					}
					return arg;
				})
				.toArray());
		return pmi.proceed();
	}
}
