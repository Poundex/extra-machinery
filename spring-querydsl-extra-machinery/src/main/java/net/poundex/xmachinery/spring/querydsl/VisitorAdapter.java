package net.poundex.xmachinery.spring.querydsl;

import com.querydsl.core.types.Constant;
import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Operation;
import com.querydsl.core.types.ParamExpression;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.SubQueryExpression;
import com.querydsl.core.types.TemplateExpression;
import com.querydsl.core.types.Visitor;

import java.util.Objects;

public class VisitorAdapter<R> implements Visitor<R, Object> {
	
	protected R getNull() {
		return null;
	}
	
	@Override
	public R visit(Constant<?> expr, Object context) {
		return getNull();
	}

	@Override
	public R visit(FactoryExpression<?> expr, Object context) {
		return getNull();
	}

	@Override
	public R visit(Operation<?> expr, Object context) {
		return expr.getArgs().stream()
				.map(expr2 -> expr2.accept(this, context))
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(getNull());
	}

	@Override
	public R visit(ParamExpression<?> expr, Object context) {
		return getNull();
	}

	@Override
	public R visit(Path<?> expr, Object context) {
		return getNull();
	}

	@Override
	public R visit(SubQueryExpression<?> expr, Object context) {
		return getNull();
	}

	@Override
	public R visit(TemplateExpression<?> expr, Object context) {
		return getNull();
	}
}
