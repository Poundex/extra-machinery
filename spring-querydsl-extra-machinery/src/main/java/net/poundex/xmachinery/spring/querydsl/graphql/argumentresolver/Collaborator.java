package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.PARAMETER})
@Repeatable(Collaborator.Collaborators.class)
public @interface Collaborator {
	String value();
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(value = {ElementType.PARAMETER})
	@Documented
	@interface Collaborators {
		Collaborator[] value();
	}
}
