package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.support.ReplaceVisitor;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Operation;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.function.TriFunction;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class QueryModifier<T, E extends Expression<?>> {
	
	private final Set<E> paths;
	private final TriFunction<T, BooleanBuilder, E, Predicate> fn;
	
	
	@SafeVarargs
	public static <EE extends Expression<?>> QueryModifierBuilder<EE> forPaths(EE... paths) {
		return new QueryModifierBuilder<>(Arrays.stream(paths).collect(Collectors.toSet()));
	}

	public BoundQueryModifier<T, E> bind(Object value) {
		return new BoundQueryModifier<>(value, paths, fn);
	}

	@RequiredArgsConstructor
	public static class QueryModifierBuilder<E extends Expression<?>> {
		private final Set<E> paths;
		
		public <T> QueryModifier<T, E> modify(TriFunction<T, BooleanBuilder, E, Predicate> fn) {
			return new QueryModifier<>(paths, fn);
		}
	}
	
	@RequiredArgsConstructor
	public static class BoundQueryModifier<T, E extends Expression<?>> {
		private final Object value;
		private final Set<E> paths;
		private final TriFunction<T, BooleanBuilder, E, Predicate> fn;

		public Predicate modify(Predicate predicate) {
			return Optional.ofNullable(doModify(predicate)).orElse(predicate);
		}
		
		private Predicate doModify(Predicate predicate) {
			return (Predicate) predicate.accept(new ReplaceVisitor<>() {
				@Override
				public Expression<?> visit(Operation<?> expr, Object context) {
					if (expr.getArgs().get(0) instanceof Path<?> exprPath 
							&& expr instanceof Predicate p 
							&& paths.contains(exprPath)) {
						return fn.apply(
								(T) value, 
								new BooleanBuilder(p),
								paths.stream()
										.filter(o -> o.equals(exprPath))
										.findFirst()
										.get());
					}
					return super.visit(expr, context);
				}
			}, null);
		}
	}
}
