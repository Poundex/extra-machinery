package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Predicate;
import graphql.schema.DataFetchingEnvironment;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.xmachinery.utils.ReflectionUtil;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.QuerydslPredicateBuilder;
import org.springframework.data.util.TypeInformation;
import org.springframework.graphql.data.method.HandlerMethodArgumentResolver;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class QueryDslArgumentResolver implements HandlerMethodArgumentResolver, EntityPathResolver {

	private final QuerydslPredicateBuilder builder = new
			ModifyingQueryDslPredicateBuilder(DefaultConversionService.getSharedInstance(), SimpleEntityPathResolver.INSTANCE);
	
	private final Map<CollaboratorKey, QueryCollaborator<?, ?>> collaborators = new HashMap<>();
	private final Map<CollaboratorKey, QueryModifier<?, ?>> modifiers = new HashMap<>();

	private final AutowireCapableBeanFactory beanFactory;

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(Root.class);
	}

	@Override
	public Mono<Predicate> resolveArgument(MethodParameter parameter, DataFetchingEnvironment environment) throws Exception {
		MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		QuerydslBindings bindings = new QuerydslBindings();

		Class<?> domainClass = parameter.getParameterAnnotation(Root.class).value();

		Map<String, Object> argumentValues = getArgumentValues(environment);
		for (Map.Entry<String, Object> entry : argumentValues.entrySet()) {
			Object value = entry.getValue();
			List<Object> values = (value instanceof List ? (List<Object>) value : Collections.singletonList(value));
			parameters.put(entry.getKey(), values);
		}
		Predicate origPredicate = builder.getPredicate(TypeInformation.of(domainClass), parameters, bindings);

		Mono<Predicate> afterCollab = io.vavr.collection.Stream.ofAll(getCollaborators(parameter, argumentValues))
				.foldLeft(Mono.just(origPredicate), (last, qc) -> last.flatMap(qc::doCollaborate));

		return io.vavr.collection.Stream.ofAll(getModifiers(parameter, argumentValues))
				.foldLeft(afterCollab, (last, qm) -> last.map(qm::modify));
	}

	private record CollaboratorKey(String collaboratorName, Class<?> klass) {}
	@SuppressWarnings("deprecation")
	private Stream<QueryCollaborator.BoundQueryCollaborator<?, ?>> getCollaborators(MethodParameter parameter, Map<String, Object> argumentValues) {
		return AnnotationUtils.getDeclaredRepeatableAnnotations(parameter.getParameter(), 
						Collaborator.class).stream()
				.filter(t -> argumentValues.containsKey(t.value()))
				.map(c -> getCollaborator(c.value(), parameter.getMethod().getDeclaringClass(), argumentValues.get(c.value())))
				.filter(Optional::isPresent)
				.map(Optional::get);
	}

	private Optional<QueryCollaborator.BoundQueryCollaborator<?, ?>> getCollaborator(
			String name, Class<?> definitionClass, Object value) {
		return Optional.ofNullable(collaborators.computeIfAbsent(new CollaboratorKey(name, definitionClass), kk -> (QueryCollaborator<?, ?>) Try.of(() ->
								ReflectionUtil.invokeMethod(kk.klass().getDeclaredMethod(kk.collaboratorName()), beanFactory.getBean(definitionClass)))
						.getOrNull()))
				.map(qc -> qc.bind(value));
	}

	@SuppressWarnings("deprecation")
	private Stream<QueryModifier.BoundQueryModifier<?, ?>> getModifiers(MethodParameter parameter, Map<String, Object> argumentValues) {
		return AnnotationUtils.getDeclaredRepeatableAnnotations(parameter.getParameter(),
						net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver.Modifier.class).stream()
				.filter(t -> argumentValues.containsKey(t.value()))
				.map(c -> getModifier(c.value(), parameter.getMethod().getDeclaringClass(), argumentValues.get(c.value())))
				.filter(Optional::isPresent)
				.map(Optional::get);
	}

	private Optional<QueryModifier.BoundQueryModifier<?, ?>> getModifier(
			String name, Class<?> definitionClass, Object value) {
		return Optional.ofNullable(modifiers.computeIfAbsent(new CollaboratorKey(name, definitionClass), kk -> (QueryModifier<?, ?>) Try.of(() ->
								ReflectionUtil.invokeMethod(kk.klass().getDeclaredMethod(kk.collaboratorName()), beanFactory.getBean(definitionClass)))
						.getOrNull()))
				.map(qm -> qm.bind(value));
	}
	
	private EntityPath<?> getQueryType(Class<?> klass) {
		return getStaticFieldOfType(klass)
				.map(it -> (EntityPath<?>) ReflectionUtils.getField(it, null))
				.get();
	}

	private Option<? extends Field> getStaticFieldOfType(Class<?> type) {
		return io.vavr.collection.Stream.of(type.getDeclaredFields())
				.filter(f -> Modifier.isStatic(f.getModifiers()))
				.filter(f -> type.equals(f.getType()))
				.headOption()
				.orElse(() -> Object.class.equals(type.getSuperclass()) 
						? Option.none() 
						: getStaticFieldOfType(type.getSuperclass()));
	}

	private static Map<String, Object> getArgumentValues(DataFetchingEnvironment environment) {
		Map<String, Object> arguments = environment.getArguments();
		if (environment.getFieldDefinition().getArguments().size() == 1) {
			String name = environment.getFieldDefinition().getArguments().get(0).getName();
			Object value = arguments.get(name);
			if (value instanceof Map<?, ?>) {
				return (Map<String, Object>) value;
			}
		}
		return arguments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> EntityPath<T> createPath(Class<T> domainClass) {
		return (EntityPath<T>) getQueryType(ReflectionUtil.classForName(String.format("%s.mongo.QMongo%s", domainClass.getPackage().getName(), domainClass.getSimpleName()), getClass().getClassLoader()));
	}
}
