package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.function.TriFunction;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class QueryCollaborator<T, R> {
	
	private final TriFunction<T, BooleanBuilder, R, Predicate> predicateFn;
	private final Function<T, Mono<R>> dataFn;
	
	public static <TT, RR> CollaboratorBuilder<TT, RR> withData(Function<TT, Mono<RR>> dataFn) {
		return new CollaboratorBuilder<>(dataFn);
	}

	public static <TT, RR> QueryCollaborator<TT, RR> collaborate(TriFunction<TT, BooleanBuilder, RR, Predicate> predicateFn) {
		return QueryCollaborator
				.<TT, RR>withData(ignored -> Mono.empty())
				.collaborate(predicateFn);
	}

	public BoundQueryCollaborator<T, R> bind(Object value) {
		return new BoundQueryCollaborator<>(value, predicateFn, dataFn);
	}

	@RequiredArgsConstructor
	public static class CollaboratorBuilder<T, R> {
		private final Function<T, Mono<R>> s;
		
		public QueryCollaborator<T, R> collaborate(TriFunction<T, BooleanBuilder, R, Predicate> predicateFn) {
			return new QueryCollaborator<>(predicateFn, s);
		}
	}
	
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class BoundQueryCollaborator<T, R> {
		private final Object value;
		private final TriFunction<T, BooleanBuilder, R, Predicate> predicateFn;
		private final Function<T, Mono<R>> dataFn;
		
		@SuppressWarnings("unchecked")
		public Mono<Predicate> doCollaborate(Predicate original) {
			Function<R, Predicate> predFn = data -> predicateFn.apply((T) value, new BooleanBuilder(original), data);
			return dataFn.apply((T) value)
					.map(predFn)
					.switchIfEmpty(Mono.fromCallable(() -> predFn.apply(null)));
		}
	}
}
