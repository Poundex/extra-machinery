package net.poundex.xmachinery.spring.querydsl.predicatemapper;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@Configuration
class PredicateMappingConfiguration {
	@Bean
	public BeanPostProcessor beanPostProcessor() {
		return new BeanPostProcessor() {
			@Override
			public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
				if(bean instanceof ReactiveQuerydslPredicateExecutor<?> &&
						AnnotationUtils.findAnnotation(bean.getClass(), QueryDslPredicateMapper.class) != null) {
					ProxyFactory proxyFactory = new ProxyFactory(bean);
					proxyFactory.addAdvice(new PredicateMappingInterceptor(bean));
					return proxyFactory.getProxy();
				}
				return bean;
			}
		};
	}
}
