package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ComparableEntityPath;
import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimplePath;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

@RequiredArgsConstructor
class FieldModifier<T, P extends Path<?>> {

	private static final Function<String, FieldModifier<Comparable<?>, ComparableEntityPath<Comparable<?>>>> fromFactory = (String variable) ->
			new FieldModifier<>(
					variable,
					Expressions::comparableEntityPath,
					ComparableExpression::goe);
	
	private static final Function<String, FieldModifier<Comparable<?>, ComparableEntityPath<Comparable<?>>>> toFactory = (String variable) ->
			new FieldModifier<>(
					variable,
					Expressions::comparableEntityPath,
					ComparableExpression::lt);
	
	private static final Function<String, FieldModifier<Object, SimplePath<Object>>> inFactory = (String variable) ->
			new FieldModifier<>(
					variable,
					Expressions::simplePath,
					(p, t) -> p.in(t instanceof Collection<?> c ? c : Collections.singletonList(t)));
					
	public static Optional<FieldModifier<?, ?>> findForField(String key) {
		return match("From", key, fromFactory::apply)
				.or(() -> match("To", key, toFactory::apply))
				.or(() -> match("In", key, inFactory::apply));
	}

	private static Optional<FieldModifier<?, ?>> match(String str, String key, Function<String, FieldModifier<?, ?>> fn) {
		if(key.endsWith(str))
			return Optional.of(fn.apply(key.substring(0, key.length() - str.length())));
		return Optional.empty();
	}

	@Getter
	private final String variable;
	private final BiFunction<Class<T>, String, P> pathFactory;
	private final BiFunction<P, T, BooleanExpression> fn;

	@SuppressWarnings("unchecked")
	public BooleanExpression apply(Object rawValue, Class<?> targetType) {
		return fn.apply(pathFactory.apply((Class<T>) targetType, variable), convert(rawValue, targetType));
	}

	@SuppressWarnings("unchecked")
	private static <T> T convert(Object value, Class<?> type) {
		if (Instant.class.isAssignableFrom(type) && value instanceof LocalDate ld)
			return (T) ld.atStartOfDay().atZone(ZoneId.of("UTC")).toInstant();

		return (T) value;
	}
}
