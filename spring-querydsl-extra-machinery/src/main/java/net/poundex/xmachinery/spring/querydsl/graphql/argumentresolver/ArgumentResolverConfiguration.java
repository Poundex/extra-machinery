package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.data.method.annotation.support.AnnotatedControllerConfigurer;

@Configuration
class ArgumentResolverConfiguration {
	
	@Bean
	public QueryDslArgumentResolver queryDslArgumentResolver(AutowireCapableBeanFactory beanFactory) {
		return new QueryDslArgumentResolver(beanFactory);
	}

	@Bean
	public BeanPostProcessor controllerConfiguringProcessor(QueryDslArgumentResolver argumentResolver) {
		return new BeanPostProcessor() {
			@Override
			public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
				if(bean instanceof AnnotatedControllerConfigurer controllerConfigurer)
					controllerConfigurer.addCustomArgumentResolver(argumentResolver);
				return bean;
			}
		};
	}
}
