package net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.QuerydslPredicateBuilder;
import org.springframework.data.util.ReflectionUtils;
import org.springframework.data.util.TypeInformation;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;

class ModifyingQueryDslPredicateBuilder extends QuerydslPredicateBuilder {
	/**
	 * Creates a new {@link QuerydslPredicateBuilder} for the given {@link ConversionService} and
	 * {@link EntityPathResolver}.
	 *
	 * @param conversionService must not be {@literal null}.
	 * @param resolver          can be {@literal null}.
	 */
	public ModifyingQueryDslPredicateBuilder(ConversionService conversionService, EntityPathResolver resolver) {
		super(conversionService, resolver);
	}

	@Override
	public Predicate getPredicate(TypeInformation<?> type, MultiValueMap<String, ?> values, QuerydslBindings bindings) {
		return doGetPredicate(type, values, bindings);
	}
	
	private Predicate doGetPredicate(TypeInformation<?> type, MultiValueMap<String, ?> values, QuerydslBindings bindings) {
		Predicate predicate = super.getPredicate(type, new MultiValueMapAdapter<>(values), bindings);
		
		BooleanBuilder builder = new BooleanBuilder(predicate);

		values.forEach((key, value) -> FieldModifier.findForField(key)
				.map(m -> m.apply(
						value.size() > 1 ? value : value.getFirst(),
						ReflectionUtils.findRequiredField(type.getType(), m.getVariable()).getType()))
				.ifPresent(builder::and));
		
		return builder;
	}
}
