package net.poundex.xmachinery.utils;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.util.ReflectionUtils;

import java.beans.BeanInfo;
import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.RecordComponent;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
public abstract class SourceReader {
    
    public record TypedPropertyValue(TypeDescriptor type, Object value) { }

    public static SourceReader forType(Class<?> sourceType) {
        if (Record.class.isAssignableFrom(sourceType))
            return new RecordSourceReader(sourceType);

        return new BeanSourceReader(sourceType);
    }
    
    protected static final MethodHandles.Lookup mhLookup = MethodHandles.lookup();
    
    protected record Accessor(String propertyName, TypeDescriptor propertyType, MethodHandle method) {}
    
    protected final Class<?> sourceType;
    protected final LoadingCache<String, Option<Accessor>> propertyReaders = 
            Caffeine.newBuilder().build(this::lookupReader);

    public Optional<TypedPropertyValue> readProperty(Object source, String name) {
        return propertyReaders.get(name)
                .onEmpty(() -> log.atWarn()
                        .addKeyValue("type", source.getClass().getName())
                        .addKeyValue("property", name)
                        .log("No source for property"))
                .map(a -> new TypedPropertyValue(a.propertyType(), ReflectionUtil.invokeMethodHandle(a.method(), source)))
                .toJavaOptional();
    }

    public abstract Map<String, Object> readToMap(Object source);
    protected abstract Option<Accessor> lookupReader(String name);

    private static class BeanSourceReader extends SourceReader {
        private final BeanInfo beanInfo;

        public BeanSourceReader(Class<?> sourceType) {
            super(sourceType);
            this.beanInfo = Try.of(() -> Introspector.getBeanInfo(sourceType)).get();
        }

        @Override
        protected Option<Accessor> lookupReader(String name) {
            return Stream.of(beanInfo.getPropertyDescriptors())
                    .filter(pd -> pd.getName().equals(name))
                    .map(PropertyDescriptor::getReadMethod)
                    .peek(ReflectionUtils::makeAccessible)
                    .map(m -> new Accessor(name, new TypeDescriptor(ResolvableType.forMethodReturnType(m), null, null), ReflectionUtil.unreflect(mhLookup, m)))
                    .headOption();
        }

        @Override
        public Map<String, Object> readToMap(Object source) {
            return Arrays.stream(beanInfo.getPropertyDescriptors())
                    .filter(it -> ! it.getName().equals("class"))
                    .collect(Collectors.toMap(
                            FeatureDescriptor::getName,
                            kv -> ReflectionUtil.invokeMethod(kv.getReadMethod(), source)));
        }
    }

    private static class RecordSourceReader extends SourceReader {
        private final RecordComponent[] recordComponents;

        public RecordSourceReader(Class<?> sourceType) {
            super(sourceType);
            recordComponents = sourceType.getRecordComponents();
        }

        @Override
        protected Option<Accessor> lookupReader(String name) {
            return Stream.of(recordComponents)
                    .filter(rc -> rc.getName().equals(name))
                    .map(RecordComponent::getAccessor)
                    .map(m -> new Accessor(name, new TypeDescriptor(ResolvableType.forMethodReturnType(m), null, null), ReflectionUtil.unreflect(mhLookup, m)))
                    .headOption();
        }

        @Override
        public Map<String, Object> readToMap(Object source) {
            return Arrays.stream(recordComponents)
                    .collect(Collectors.toMap(
                            RecordComponent::getName,
                            kv -> ReflectionUtil.invokeMethod(kv.getAccessor(), source)));
        }
    }
}