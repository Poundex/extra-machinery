package net.poundex.xmachinery.utils;

import org.springframework.core.ResolvableType;
import org.springframework.util.ReflectionUtils;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public abstract class ReflectionUtil {
    
    private ReflectionUtil() { }
    
    public static <T> T invokeConstructor(Constructor<T> constructor, Object... args) {
        try {
            ReflectionUtils.makeAccessible(constructor);
            return constructor.newInstance(args);
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public static Class<?> classForName(String name, ClassLoader classLoader) {
        try {
            return Class.forName(name, true, classLoader);
        } catch (ClassNotFoundException cnfex) {
            throw new RuntimeException(cnfex);
        }
    }
    
    public static Object invokeMethod(Method method, Object target, Object... args) {
        try {
            ReflectionUtils.makeAccessible(method);
            return method.invoke(target, args);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Object invokeMethodHandle(MethodHandle mh, Object target) {
        try {
            return mh.invoke(target);
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public static MethodHandle unreflect(MethodHandles.Lookup lookup, Method method) {
        try {
            return lookup.unreflect(method);
        } catch (IllegalAccessException iaex) {
            throw new RuntimeException(iaex);
        }
    }

	public static ResolvableType findGenericSuperInterface(ResolvableType onType, Class<?> theInterface) {
	    return Arrays.stream(onType.getInterfaces())
	            .filter(t -> theInterface.isAssignableFrom(t.getRawClass()))
	            .findFirst()
	            .orElseThrow();
	}
}
