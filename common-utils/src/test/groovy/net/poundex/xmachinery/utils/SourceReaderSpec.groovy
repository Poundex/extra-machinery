package net.poundex.xmachinery.utils

import net.poundex.xmachinery.test.models.ClassOne
import net.poundex.xmachinery.test.models.RecordOne
import org.springframework.core.ResolvableType
import spock.lang.Specification
import spock.lang.Subject

class SourceReaderSpec extends Specification {
    
    void "Returns empty when reading non-existent bean property"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(ClassOne)
        ClassOne source = new ClassOne("some-string")
        
        when:
        Optional<Object> res = sourceReader.readProperty(source, "noSuchProperty")
        
        then:
        res.isEmpty()
    }

    void "Returns value when reading bean property"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(ClassOne)
        ClassOne source = new ClassOne("some-string")

        when:
        Optional<SourceReader.TypedPropertyValue> res = sourceReader.readProperty(source, "simpleString")

        then:
        res.get().type().type == ResolvableType.forClass(String).type
        res.get().value() == "some-string"
    }
    
    void "Returns empty when reading non-existent record component"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(RecordOne)
        RecordOne source = new RecordOne("some-string")

        when:
        Optional<Object> res = sourceReader.readProperty(source, "noSuchProperty")

        then:
        res.isEmpty()
    }

    void "Returns value when reading record component"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(RecordOne)
        RecordOne source = new RecordOne("some-string")

        when:
        Optional<SourceReader.TypedPropertyValue> res = sourceReader.readProperty(source, "simpleString")

        then:
        res.get().type().type == ResolvableType.forClass(String).type
        res.get().value() == "some-string"
    }
    
    void "Returns map of all properties"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(ClassOne)
        ClassOne source = new ClassOne("some-string")

        when:
        Map<String, Object> map = sourceReader.readToMap(source)

        then:
        map.size() == 1
        map['simpleString'] == "some-string"
    }
    
    void "Returns map of all properties for Records"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(RecordOne)
        RecordOne source = new RecordOne("some-string")

        when:
        Map<String, Object> map = sourceReader.readToMap(source)

        then:
        map.size() == 1
        map['simpleString'] == "some-string"
    }
}
