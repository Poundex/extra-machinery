package net.poundex.jsonlog;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.function.Supplier;

public interface ObjectMapperFactory extends Supplier<ObjectMapper> {
}
