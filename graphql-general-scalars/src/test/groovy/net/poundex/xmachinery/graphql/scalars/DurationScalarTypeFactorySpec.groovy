package net.poundex.xmachinery.graphql.scalars

import graphql.language.IntValue
import graphql.language.StringValue
import graphql.schema.CoercingParseLiteralException
import graphql.schema.CoercingParseValueException
import graphql.schema.CoercingSerializeException
import graphql.schema.GraphQLScalarType
import spock.lang.Specification
import spock.lang.Subject

import java.time.Duration

class DurationScalarTypeFactorySpec extends Specification {
	
	private static final Duration SOME_DURATION = Duration.ofMinutes(70)
	private static final Long SOME_DURATION_LONG = 70 * 60
	
	@Subject
	GraphQLScalarType scalar = new DurationScalarTypeFactory().scalarType
	
	void "Serializes Duration"() {
		expect:
		scalar.getCoercing()
				.serialize(SOME_DURATION) == SOME_DURATION_LONG

		when:
		scalar.getCoercing().serialize("")

		then:
		thrown(CoercingSerializeException)
	}

	void "Parses Duration"() {
		expect:
		scalar.getCoercing()
				.parseValue(SOME_DURATION_LONG) == SOME_DURATION

		when:
		scalar.getCoercing().parseValue("")

		then:
		thrown(CoercingParseValueException)
	}

	void "Parses Literal LocalDate"() {
		expect:
		scalar.getCoercing()
				.parseLiteral(new IntValue(SOME_DURATION_LONG.toBigInteger())) == SOME_DURATION

		when:
		scalar.getCoercing().parseLiteral(new StringValue(""))

		then:
		thrown(CoercingParseLiteralException)
	}
}
