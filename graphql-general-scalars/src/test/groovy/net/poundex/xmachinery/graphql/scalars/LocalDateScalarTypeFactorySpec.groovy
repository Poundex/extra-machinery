package net.poundex.xmachinery.graphql.scalars

import graphql.language.StringValue
import graphql.schema.CoercingParseLiteralException
import graphql.schema.CoercingParseValueException
import graphql.schema.CoercingSerializeException
import graphql.schema.GraphQLScalarType
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate

class LocalDateScalarTypeFactorySpec extends Specification {
	
	private static final LocalDate SOME_LOCAL_DATE = LocalDate.of(2023, 6, 27)
	private static final String SOME_LOCAL_DATE_STRING = "2023-06-27"
	
	@Subject
	GraphQLScalarType localDateScalar = new LocalDateScalarTypeFactory().scalarType
	
	void "Serializes LocalDate"() {
		expect:
		localDateScalar.getCoercing()
				.serialize(SOME_LOCAL_DATE) == SOME_LOCAL_DATE_STRING

		when:
		localDateScalar.getCoercing().serialize("")

		then:
		thrown(CoercingSerializeException)
	}

	void "Parses LocalDate"() {
		expect:
		localDateScalar.getCoercing()
				.parseValue(SOME_LOCAL_DATE_STRING) == SOME_LOCAL_DATE

		when:
		localDateScalar.getCoercing().parseValue("")

		then:
		thrown(CoercingParseValueException)
	}

	void "Parses Literal LocalDate"() {
		expect:
		localDateScalar.getCoercing()
				.parseLiteral(new StringValue(SOME_LOCAL_DATE_STRING)) == SOME_LOCAL_DATE

		when:
		localDateScalar.getCoercing().parseLiteral(new StringValue(""))

		then:
		thrown(CoercingParseLiteralException)
	}
}
