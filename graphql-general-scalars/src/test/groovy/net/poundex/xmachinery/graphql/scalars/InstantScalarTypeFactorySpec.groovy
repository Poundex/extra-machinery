package net.poundex.xmachinery.graphql.scalars

import graphql.language.StringValue
import graphql.schema.CoercingParseLiteralException
import graphql.schema.CoercingParseValueException
import graphql.schema.CoercingSerializeException
import graphql.schema.GraphQLScalarType
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

class InstantScalarTypeFactorySpec extends Specification {
	
	private static final Instant SOME_INSTANT = Instant.ofEpochMilli(1687890426813)
	private static final String SOME_INSTANT_STRING = "2023-06-27T18:27:06.813Z"

	@Subject
	GraphQLScalarType instantScalar = new InstantScalarTypeFactory().scalarType

	void "Serializes Instant"() {
		expect:
		instantScalar.getCoercing().serialize(SOME_INSTANT) == SOME_INSTANT_STRING

		when:
		instantScalar.getCoercing().serialize("")

		then:
		thrown(CoercingSerializeException)
	}

	void "Parses Instant"() {
		expect:
		instantScalar.getCoercing().parseValue(SOME_INSTANT_STRING) == SOME_INSTANT

		when:
		instantScalar.getCoercing().parseValue("")

		then:
		thrown(CoercingParseValueException)
	}

	void "Parses Literal Instant"() {
		expect:
		instantScalar.getCoercing().parseLiteral(new StringValue(SOME_INSTANT_STRING)) == SOME_INSTANT

		when:
		instantScalar.getCoercing().parseLiteral(new StringValue(""))

		then:
		thrown(CoercingParseLiteralException)
	}
}
