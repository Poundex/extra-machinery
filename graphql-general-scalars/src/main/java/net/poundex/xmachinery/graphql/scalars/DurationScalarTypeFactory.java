package net.poundex.xmachinery.graphql.scalars;

import graphql.language.IntValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;
import io.vavr.control.Try;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class DurationScalarTypeFactory implements GraphQLScalarTypeFactory {

	@Override
	public GraphQLScalarType getScalarType() {
		return GraphQLScalarType.newScalar()
				.name("Duration")
				.description("Duration")
				.coercing(new Coercing<Duration, Long>() {
					@Override
					public Long serialize(Object dataFetcherResult) {
						if (dataFetcherResult instanceof Duration duration)
							return duration.toSeconds();

						throw new CoercingSerializeException(String.format(
								"%s (%s) is not a Duration",
								dataFetcherResult.toString(),
								dataFetcherResult.getClass().getName()));
					}

					@Override
					public Duration parseValue(Object input) {
						return Try.of(() -> input)
								.filter(in -> in instanceof Number)
								.map(str -> Duration.ofSeconds(Long.parseLong(input.toString())))
								.getOrElseThrow(() -> new CoercingParseValueException(String.format(
										"%s (%s) could not be parsed into Duration (should be a whole number of seconds)",
										input,
										input.getClass().getName())));
					}

					@Override
					public Duration parseLiteral(Object input) {
						return Try.of(() -> input)
								.filter(in -> in instanceof IntValue)
								.map(iv -> (IntValue) iv)
								.map(v -> Duration.ofSeconds(v.getValue().longValue()))
								.getOrElseThrow(() -> new CoercingParseLiteralException(String.format(
										"%s (%s) is not a Duration IntValue",
										input,
										input.getClass().getName())));
					}
				}).build();
	}
}
