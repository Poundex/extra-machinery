package net.poundex.xmachinery.graphql.scalars;

import graphql.schema.GraphQLScalarType;

public interface GraphQLScalarTypeFactory {
	GraphQLScalarType getScalarType();
}
