package net.poundex.xmachinery.graphql.scalars;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;
import io.vavr.control.Try;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class InstantScalarTypeFactory implements GraphQLScalarTypeFactory {

	@Override
	public GraphQLScalarType getScalarType() {
		return GraphQLScalarType.newScalar()
				.name("Instant")
				.description("Instant")
				.coercing(new Coercing<Instant, String>() {
					@Override
					public String serialize(Object dataFetcherResult) {
						if (dataFetcherResult instanceof Instant inst)
							return inst.toString();

						throw new CoercingSerializeException(String.format(
								"%s (%s) is not a Instant",
								dataFetcherResult.toString(),
								dataFetcherResult.getClass().getName()));
					}

					@Override
					public Instant parseValue(Object input) {
						return Try.of(() -> input)
								.filter(in -> in instanceof String)
								.map(str -> Instant.parse(str.toString()))
								.getOrElseThrow(() -> new CoercingParseValueException(String.format(
										"%s (%s) is not a Instant string",
										input,
										input.getClass().getName())));
					}

					@Override
					public Instant parseLiteral(Object input) {
						return Try.of(() -> input)
								.filter(in -> in instanceof StringValue)
								.map(sv -> (StringValue) sv)
								.map(str -> Instant.parse(str.getValue()))
								.getOrElseThrow(() -> new CoercingParseLiteralException(String.format(
										"%s (%s) is not a Instant StringValue",
										input,
										input.getClass().getName())));
					}
				}).build();
	}
}
