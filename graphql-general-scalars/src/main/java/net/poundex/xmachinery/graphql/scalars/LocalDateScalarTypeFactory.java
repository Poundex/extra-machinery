package net.poundex.xmachinery.graphql.scalars;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;
import io.vavr.control.Try;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class LocalDateScalarTypeFactory implements GraphQLScalarTypeFactory {

	@Override
	public GraphQLScalarType getScalarType() {
		return GraphQLScalarType.newScalar()
				.name("LocalDate")
				.description("LocalDate")
				.coercing(new Coercing<LocalDate, String>() {
					@Override
					public String serialize(Object dataFetcherResult) {
						if (dataFetcherResult instanceof LocalDate ld)
							return ld.toString();

						throw new CoercingSerializeException(String.format(
								"%s (%s) is not a LocalDate",
								dataFetcherResult.toString(),
								dataFetcherResult.getClass().getName()));
					}

					@Override
					public LocalDate parseValue(Object input) {
						return Try.of(() -> input)
								.filter(in -> in instanceof String)
								.map(str -> LocalDate.parse(str.toString()))
								.getOrElseThrow(() -> new CoercingParseValueException(String.format(
										"%s (%s) is not a LocalDate string",
										input,
										input.getClass().getName())));
					}

					@Override
					public LocalDate parseLiteral(Object input) {
						return Try.of(() -> input)
								.filter(in -> in instanceof StringValue)
								.map(sv -> (StringValue) sv)
								.map(str -> LocalDate.parse(str.getValue()))
								.getOrElseThrow(() -> new CoercingParseLiteralException(String.format(
										"%s (%s) is not a LocalDate StringValue",
										input,
										input.getClass().getName())));
					}
				}).build();
	}
}
