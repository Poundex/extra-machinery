package net.poundex.xmachinery.graphql.scalars;

import graphql.schema.idl.RuntimeWiring;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

@Configuration
@ComponentScan
@RequiredArgsConstructor
public class ScalarConfiguration implements RuntimeWiringConfigurer {
	
	private final ObjectProvider<GraphQLScalarTypeFactory> scalarFactories;
	
	@Override
	public void configure(RuntimeWiring.Builder builder) {
		scalarFactories.forEach(f -> builder.scalar(f.getScalarType()));
	}
}
