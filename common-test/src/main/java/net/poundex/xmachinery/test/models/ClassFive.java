package net.poundex.xmachinery.test.models;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClassFive implements ClassFiveOrSix {
    private final String simpleString;
}
