package net.poundex.xmachinery.test.models;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClassSix implements ClassFiveOrSix {
    private final String simpleString;
}
