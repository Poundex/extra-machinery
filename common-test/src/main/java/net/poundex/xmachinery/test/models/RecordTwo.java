package net.poundex.xmachinery.test.models;

public record RecordTwo(String simpleString, int skip) {
}
