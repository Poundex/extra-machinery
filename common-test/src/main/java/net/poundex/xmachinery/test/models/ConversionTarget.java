package net.poundex.xmachinery.test.models;

public record ConversionTarget(String string, String integer) {
}
