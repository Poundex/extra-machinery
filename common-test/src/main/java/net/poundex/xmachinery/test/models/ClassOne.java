package net.poundex.xmachinery.test.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ClassOne {
    private final String simpleString;
}
