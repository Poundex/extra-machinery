package net.poundex.xmachinery.test.models;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClassTwo {
    private final String simpleString;
    private final int skip;
}
