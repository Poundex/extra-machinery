package net.poundex.xmachinery.test.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ClassSeven {
    private final int number;
    private final String string;
    private final int stringFromNumber;
}
