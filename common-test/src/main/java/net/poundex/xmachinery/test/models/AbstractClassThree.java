package net.poundex.xmachinery.test.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public abstract class AbstractClassThree {
    private final String simpleString;
}
