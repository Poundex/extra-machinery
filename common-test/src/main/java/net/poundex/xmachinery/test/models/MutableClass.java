package net.poundex.xmachinery.test.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MutableClass {
    private int number;
    private String string;
    private String stringFromNumber;
}
