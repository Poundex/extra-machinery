package net.poundex.xmachinery.test.models;

public record ConversionSource(String string, Integer integer) {
}
