package net.poundex.xmachinery.spring.graphql.batch


import groovy.transform.CompileStatic
import groovy.transform.Immutable
import groovy.transform.TupleConstructor
import org.dataloader.BatchLoaderEnvironment
import org.springframework.graphql.execution.BatchLoaderRegistry
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.BiFunction

class BatchLoadingPostProcessorSpec extends Specification {
	
	@Immutable
	private static class SomeDomainObject {
		final Long id
	}
	
	private interface SomeRepository {
		Flux<SomeDomainObject> findAllById(Set<Long> ids);
	}
	
	@CompileStatic
	@TupleConstructor
	@RegisterBatchLoaders
	private static class SomeService {
		
		final SomeRepository repository
		
		@BatchLoader
		Mono<Map<Long, SomeDomainObject>> simpleBatchLoader(Set<Long> ids) {
			return repository.findAllById(ids)
					.collectMap { it.id }
		}
		
		@BatchLoader
		Mono<Map<Long, SomeDomainObject>> withEnv1(Set<Long> ids, BatchLoaderEnvironment env) {
			env.getContext()
			return repository.findAllById(ids)
					.collectMap { it.id }
		}
		
		@BatchLoader
		Mono<Map<Long, SomeDomainObject>> withEnv2(BatchLoaderEnvironment env, Set<Long> ids) {
			env.getContext()
			return repository.findAllById(ids)
					.collectMap { it.id }
		}
	}
	
	SomeDomainObject domain1 = new SomeDomainObject(1L)
	SomeDomainObject domain2 = new SomeDomainObject(2L)
	SomeDomainObject domain3 = new SomeDomainObject(3L)
	
	SomeRepository repository = Stub() {
		findAllById(_ as Set<Long>) >> { Set<Long> ids ->
			return Flux.fromIterable(ids.collect { new SomeDomainObject(it) })
		}
	}
	SomeService service = new SomeService(repository)
	
	BatchLoaderRegistry batchLoaderRegistry = Mock()
	BatchLoaderRegistry.RegistrationSpec<Long, SomeDomainObject> registrationSpec = Mock()
	BatchLoaderEnvironment batchLoaderEnvironment = Mock()
	
	@Subject
	BatchLoadingPostProcessor postProcessor = new BatchLoadingPostProcessor(batchLoaderRegistry, Stub(ObjectLoaderArgumentResolver), Stub(UniverseLoaderArgumentResolver))
	
	void "Registers simple batch loaders"() {
		when:
		def returned = postProcessor.postProcessAfterInitialization(service, "")
		
		then:
		returned.is(service)
		
		and: 
		3 * batchLoaderRegistry.forTypePair(Long.class, SomeDomainObject.class) >> registrationSpec
		3 * registrationSpec.withName(SomeDomainObject.class.name) >> registrationSpec
		3 * registrationSpec.registerMappedBatchLoader({ BiFunction<Set<Long>, BatchLoaderEnvironment, Mono<Map<Long, SomeDomainObject>>> fn -> 
			fn.apply([1L, 2L, 3L].toSet(), batchLoaderEnvironment).block() == [
			        1L: domain1,
			        2L: domain2,
			        3L: domain3
			]
		})
	}
	
	void "Provides environment when requested"() {
		when:
		def returned = postProcessor.postProcessAfterInitialization(service, "")

		then:
		returned.is(service)

		and:
		3 * batchLoaderRegistry.forTypePair(Long.class, SomeDomainObject.class) >> registrationSpec
		3 * registrationSpec.withName(SomeDomainObject.class.name) >> registrationSpec
		3 * registrationSpec.registerMappedBatchLoader(
				_ as BiFunction<Set<Long>, BatchLoaderEnvironment, Mono<Map<Long, SomeDomainObject>>>) >>
				{ BiFunction<Set<Long>, BatchLoaderEnvironment, Mono<Map<Long, SomeDomainObject>>> fn -> 
					fn.apply([1L, 2L, 3L].toSet(), batchLoaderEnvironment)
				}
		2 * batchLoaderEnvironment.getContext()
	}
}
