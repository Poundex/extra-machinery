package net.poundex.xmachinery.spring.graphql.batch

import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Function

class BatchLoadingHelperSpec extends Specification implements BatchLoadingUtils, PublisherUtils {

	private static final someModel = new Object()
	private static final someDomainObject = new Object()
	
	ObjectLoader<String, Object> dataLoader = Stub()
	
	@Subject
	BatchLoadingHelper batchLoadingHelper = new BatchLoadingHelper()
	
	void "Return else case when field set contains only ID"() {
		when:
		Object r = block batchLoadingHelper.loadIfNecessary("1", justId(), dataLoader)
				.andMap({ null })
				.orElse({ -> someModel })
		
		then:
		r.is someModel
	}
	

	void "Load data and return mapped result when other fields present"() {
		given:
		dataLoader.load("1") >> Mono.just(someDomainObject)
		Function<Object, Object> mapper = Mock()
		
		when:
		Object r = block batchLoadingHelper.loadIfNecessary("1", fields("name"), dataLoader)
				.andMap(mapper)
				.orElse({ null })

		then:
		r.is someModel
		1 * mapper.apply(someDomainObject) >> someModel
	}
}
