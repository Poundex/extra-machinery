package net.poundex.xmachinery.spring.graphql.error

import graphql.ErrorClassification
import graphql.GraphQLError
import graphql.schema.DataFetchingEnvironment
import groovy.transform.InheritConstructors
import jakarta.validation.ConstraintViolation
import jakarta.validation.ConstraintViolationException
import jakarta.validation.Path
import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.graphql.execution.ErrorType
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.BiFunction
import java.util.stream.Stream

class ExceptionResolverSpec extends Specification implements PublisherUtils {

	DataFetchingEnvironment env = Stub()
	
	@InheritConstructors
	private static class SomeException extends RuntimeException {}

	BiFunction<SomeException, DataFetchingEnvironment, Mono<List<GraphQLError>>> customErrorFn = Stub()

	@Subject
	ExceptionResolver exceptionResolver = new ExceptionResolver(Stream.of(new ExceptionHandler<>(SomeException, customErrorFn)))

	void "Resolve NotFoundException"() {
		when:
		List<GraphQLError> errors = block exceptionResolver.resolveException(new NotFoundException(Object.class, "123"), env)

		then:
		errors.size() == 1
		with(errors.first()) {
			errorType == ErrorType.NOT_FOUND
		}
	}

	void "Handle field errors"() {
		given:
		ConstraintViolation cv1 = Stub() {
			getPropertyPath() >> Stub(Path) {
				toString() >> "foo.bar"
			}
		}
		ConstraintViolation cv2 = Stub() {
			getPropertyPath() >> Stub(Path) {
				toString() >> "foo.baz"
			}
		}

		when:
		List<GraphQLError> errors = block exceptionResolver
				.resolveException(new ConstraintViolationException(
						Set.of(cv1, cv2)), env)

		then:
		errors.size() == 1
		with(errors.first()) {
			errorType.toString() == ErrorClassification.errorClassification("UNPROCESSABLE_ENTITY").toSpecification()
//			extensions.error == "UNPROCESSABLE_ENTITY"
			with((List<Map<String, Object>>) extensions.validationErrors) {
				size() == 2
				it.find {
					it.type == "FIELD_ERROR" &&
							it.path == "foo.bar"
				}
				it.find {
					it.type == "FIELD_ERROR" &&
							it.path == "foo.baz"
				}
			}
		}
	}
	
	void "Custom handler"() {
		given:
		List<GraphQLError> expected = Stub()
		customErrorFn.apply(_, _) >> Mono.just(expected)
		
		when:
		List<GraphQLError> errors = block exceptionResolver.resolveException(new SomeException(), env)
		
		then:
		errors == expected
	}
}
