package net.poundex.xmachinery.spring.graphql.error;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class MultipleRequestErrorsException extends RuntimeException {
	private final List<RuntimeException> exceptions;
}
