package net.poundex.xmachinery.spring.graphql.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.netflix.graphql.dgs.client.GraphQLResponse;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import graphql.ErrorClassification;
import graphql.GraphQLError;
import graphql.execution.ResultPath;
import graphql.language.SourceLocation;
import io.vavr.control.Try;
import net.poundex.xmachinery.spring.graphql.error.MultipleRequestErrorsException;
import net.poundex.xmachinery.spring.graphql.error.NotFoundException;
import org.intellij.lang.annotations.Language;
import org.springframework.graphql.execution.ErrorType;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ErrorHandlingGraphQlClientDecorator implements MonoGraphQLClient {

	public static final TypeReference<Map<String, Object>> MAP_STRING_TO_OBJECT = new TypeReference<>() {};

	public static MonoGraphQLClient wrap(MonoGraphQLClient client, ObjectMapper objectMapper) {
		return wrap(client, objectMapper, Collections.emptyMap());
	}

	public static MonoGraphQLClient wrap(MonoGraphQLClient client, ObjectMapper objectMapper, Map<ErrorClassification, Function<GraphQLError, ? extends RuntimeException>> handlers) {
		return new ErrorHandlingGraphQlClientDecorator(client, objectMapper, handlers);
	}
	
	private final MonoGraphQLClient delegate;
	private final ObjectMapper objectMapper;
	private final Map<ErrorClassification, Function<GraphQLError, ? extends RuntimeException>> handlers;
	private final Map<String, ErrorClassification> errorClassifications;

	private ErrorHandlingGraphQlClientDecorator(MonoGraphQLClient delegate, ObjectMapper objectMapper, Map<ErrorClassification, Function<GraphQLError, ? extends RuntimeException>> handlers) {
		this.delegate = delegate;
		this.objectMapper = objectMapper;
		this.handlers = Stream.concat(standardHandlers().entrySet().stream(), handlers.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		this.errorClassifications = errorClassifications(this.handlers);
	}

	private static Map<ErrorClassification, Function<GraphQLError, ? extends RuntimeException>> standardHandlers() {
		return Map.of(
				ErrorType.NOT_FOUND,
				graphQLError -> new NotFoundException(
						graphQLError.getExtensions().get("type").toString(),
						graphQLError.getExtensions().get("key").toString()));
	}

	private static Map<String, ErrorClassification> errorClassifications(Map<ErrorClassification, Function<GraphQLError, ? extends RuntimeException>> handlers) {
		return Stream.concat(
				Arrays.stream(graphql.ErrorType.values()), Stream.concat(
						Arrays.stream(ErrorType.values()),
						handlers.keySet().stream()))
				.collect(Collectors.toMap(Object::toString, ec -> ec, (l, r) -> r));
	}

	@Override
	public Mono<GraphQLResponse> reactiveExecuteQuery(@Language("graphql") String query) {
		return delegate.reactiveExecuteQuery(query)
				.map(this::checkErrors);
	}

	@Override
	public Mono<GraphQLResponse> reactiveExecuteQuery(@Language("graphql") String query, Map<String, ?> map) {
		return delegate.reactiveExecuteQuery(query, map)
				.map(this::checkErrors);
	}

	@Override
	public Mono<GraphQLResponse> reactiveExecuteQuery(@Language("graphql") String query, Map<String, ?> map, String operationName) {
		return delegate.reactiveExecuteQuery(query, map, operationName)
				.map(this::checkErrors);
	}

	private GraphQLResponse checkErrors(GraphQLResponse graphQLResponse) {
		if ( ! graphQLResponse.hasErrors())
			return graphQLResponse;

		List<RuntimeException> exceptions = Try.of(() -> {
					ArrayNode errors = objectMapper.readTree(graphQLResponse.getJson()).withArray("errors");
					return stream(errors.elements())
							.map(err -> {
								Map<String, Object> extensions = getExtensions(err);
								return GraphQLError.newError()
										.message(err.get("message").textValue())
										.locations(stream(err.withArray("locations").elements())
												.map(loc -> new SourceLocation(loc.get("line").asInt(), loc.get("column").asInt()))
												.toList())
										.path(ResultPath.fromList(stream(err.withArray("path").elements())
												.map(JsonNode::textValue)
												.toList()))
										.extensions(extensions)
										.errorType(errorClassifications.getOrDefault(extensions.get("classification").toString(), 
												ErrorClassification.errorClassification(extensions.get("classification").toString())))
										.build();
							})
							.toList();
				})
				.get()
				.stream()
				.map(this::mapError)
				.toList();
		
		if(exceptions.size() == 1)
			throw exceptions.get(0);
		
		throw new MultipleRequestErrorsException(exceptions);
	}

	private Map<String, Object> getExtensions(JsonNode err) {
		return Try.of(() -> objectMapper.readValue(objectMapper.treeAsTokens(err.withObject("/extensions")),
				MAP_STRING_TO_OBJECT))
				.get();
	}

	private static final Function<GraphQLError, ? extends RuntimeException> defaultHandler = 
			err -> { throw new RuntimeException(err.toString()); };
	
	private RuntimeException mapError(GraphQLError graphQLError) {
		return handlers.getOrDefault(graphQLError.getErrorType(), defaultHandler).apply(graphQLError);
	}
	
	private static <T> Stream<T> stream(Iterator<T> iterator) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
	}
}
