package net.poundex.xmachinery.spring.graphql.batch;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.execution.BatchLoaderRegistry;

@Configuration
@RequiredArgsConstructor
public class BatchLoadingConfiguration {
	
	@Bean
	ObjectLoaderArgumentResolver objectLoaderArgumentResolver() {
		return new ObjectLoaderArgumentResolver();
	}
	
	@Bean
	UniverseLoaderArgumentResolver universeLoaderArgumentResolver() {
		return new UniverseLoaderArgumentResolver();
	}
	
	@Bean
	BatchLoadingPostProcessor postProcessor(
			BatchLoaderRegistry registry, 
			ObjectLoaderArgumentResolver objectLoaderArgumentResolver, 
			UniverseLoaderArgumentResolver universeLoaderArgumentResolver) {
		return new BatchLoadingPostProcessor(registry, objectLoaderArgumentResolver, universeLoaderArgumentResolver);
	}
}
