package net.poundex.xmachinery.spring.graphql.client;

import com.netflix.graphql.dgs.client.GraphQLResponse;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import graphql.language.ListType;
import graphql.language.NonNullType;
import graphql.language.OperationDefinition;
import graphql.language.Type;
import graphql.language.TypeName;
import graphql.language.VariableDefinition;
import graphql.parser.Parser;
import graphql.schema.Coercing;
import graphql.schema.GraphQLInputObjectField;
import graphql.schema.GraphQLInputObjectType;
import graphql.schema.GraphQLNamedSchemaElement;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLScalarType;
import graphql.schema.GraphQLType;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import net.poundex.xmachinery.utils.SourceReader;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.Nullable;
import org.springframework.graphql.execution.GraphQlSource;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ArgumentCoercingGraphQlClientDecorator implements MonoGraphQLClient {
	
	public static MonoGraphQLClient wrap(MonoGraphQLClient client, GraphQlSource source) {
		return new ArgumentCoercingGraphQlClientDecorator(
				client, 
				getTypeCoercers(source), 
				getInputObjectDefinitions(source));
	}
	
	private static final Set<String> BUILT_INS = Set.of("ID", "String", "Boolean", "Int");

	private final MonoGraphQLClient delegate;

	private final Map<String, Coercing<?, ?>> typeCoercers;
	private final Map<String, GraphQLInputObjectType> objectTypes;
	private final Map<Class<?>, SourceReader> sourceReaders = new HashMap<>();
	
	@Override
	public Mono<GraphQLResponse> reactiveExecuteQuery(@Language("graphql") String query) {
		return delegate.reactiveExecuteQuery(query);
	}

	@Override
	public Mono<GraphQLResponse> reactiveExecuteQuery(
			@Language("graphql") String query, 
			Map<String, ?> args) {
		return delegate.reactiveExecuteQuery(query, coerceArgs(args, query));
	}

	@Override
	public Mono<GraphQLResponse> reactiveExecuteQuery(
			@Language("graphql") String query, 
			Map<String, ?> args, 
			@Nullable String operationName) {
		return delegate.reactiveExecuteQuery(query, coerceArgs(args, query), operationName);
	}

	private static Map<String, Coercing<?, ?>> getTypeCoercers(GraphQlSource source) {
		return source.schema().getAllTypesAsList()
				.stream()
				.filter(it -> it instanceof GraphQLScalarType)
				.map(it -> (GraphQLScalarType) it)
				.filter(it -> ! BUILT_INS.contains(it.getName()))
				.collect(Collectors.toMap(
						GraphQLScalarType::getName, 
						GraphQLScalarType::getCoercing));
	}

	private static Map<String, GraphQLInputObjectType> getInputObjectDefinitions(GraphQlSource source) {
		return source.schema().getAllTypesAsList()
				.stream()
				.filter(it -> it instanceof GraphQLInputObjectType)
				.map(it -> (GraphQLInputObjectType) it)
				.collect(Collectors.toMap(
						GraphQLInputObjectType::getName,
						kv -> kv));
	}

	private Map<String, ?> coerceArgs(Map<String, ?> inputArgMap, String query) {
		Map<String, String> typedVariables = Parser.parse(query).getDefinitions().stream()
				.filter(it -> it instanceof OperationDefinition)
				.map(it -> (OperationDefinition) it)
				.flatMap(it -> it.getVariableDefinitions().stream())
				.collect(Collectors.toMap(
						VariableDefinition::getName,
						kv -> unwrapType(kv.getType()).getName()));
		
		return getValues(inputArgMap, typedVariables);
	}

	private Map<String, ?> getValues(Map<String, ?> inputArgMap, Map<String, String> typedVariables) {
		return inputArgMap.entrySet().stream()
				.filter(kv -> kv.getValue() != null)
				.map(arg -> getValue(arg, typedVariables))
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						Map.Entry::getValue));
	}

	private Map.Entry<String, ?> getValue(Map.Entry<String, ?> arg, Map<String, String> typedVariables) {
		if(typeCoercers.containsKey(typedVariables.get(arg.getKey())))
			return Map.entry(
					arg.getKey(), 
					typeCoercers.get(typedVariables.get(arg.getKey())).serialize(arg.getValue()));
		
		if(objectTypes.containsKey(typedVariables.get(arg.getKey())))
			return Map.entry(
					arg.getKey(), 
					getObjectValue(objectTypes.get(typedVariables.get(arg.getKey())), arg.getValue()));
		
		return arg;
	}

	private Object getObjectValue(GraphQLInputObjectType type, Object value) {
		Map<String, String> objectPropertyToName = type.getFields().stream()
				.collect(Collectors.toMap(
						GraphQLInputObjectField::getName,
						kv -> unwrapType(kv.getType()).getName()));
		
		if(value instanceof Map m) 
			return getValues(m, objectPropertyToName);

		if( ! sourceReaders.containsKey(value.getClass())) 
			sourceReaders.put(value.getClass(), SourceReader.forType(value.getClass()));
		
		return getValues(sourceReaders.get(value.getClass()).readToMap(value), objectPropertyToName);
	}

	private static TypeName unwrapType(Type<?> type) {
		if(type instanceof NonNullType nnt)
			type = nnt.getType();
		if(type instanceof ListType listType)
			return unwrapType(listType.getType());
		
		return (TypeName) type;
	}
	
	private static GraphQLNamedSchemaElement unwrapType(GraphQLType type) {
		if(type instanceof GraphQLNonNull gnn)
			type = gnn.getWrappedType();

		return (GraphQLNamedSchemaElement) type;
	}	
}
