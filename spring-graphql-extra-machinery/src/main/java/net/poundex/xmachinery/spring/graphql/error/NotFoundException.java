package net.poundex.xmachinery.spring.graphql.error;

import graphql.ErrorClassification;
import lombok.Getter;
import org.springframework.graphql.execution.ErrorType;

import java.util.Map;

@Getter
public class NotFoundException extends GraphQlRequestException {
	
	private final String type;
	private final String key;

	public NotFoundException(String type, String key) {
		super(String.format("%s with key %s not found", type, key));
		this.type = type;
		this.key = key;
	}
	
	public NotFoundException(Class<?> type, String key) {
		this(type.getSimpleName(), key);
	}

	@Override
	public ErrorClassification getErrorType() {
		return ErrorType.NOT_FOUND;
	}

	@Override
	public Map<String, Object> getExtensions() {
		return Map.of(
				"type", String.valueOf(type),
				"key", String.valueOf(key));
	}
}
