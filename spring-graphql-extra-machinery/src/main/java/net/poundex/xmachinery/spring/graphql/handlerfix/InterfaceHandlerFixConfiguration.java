package net.poundex.xmachinery.spring.graphql.handlerfix;

import graphql.language.FieldDefinition;
import graphql.language.NamedNode;
import graphql.language.ObjectTypeDefinition;
import graphql.schema.DataFetcher;
import graphql.schema.idl.FieldWiringEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.WiringFactory;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.data.method.annotation.support.AnnotatedControllerConfigurer;
import org.springframework.graphql.execution.BatchLoaderRegistry;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Configuration
public class InterfaceHandlerFixConfiguration {

	@Bean
	public RuntimeWiringConfigurer runtimeWiringConfigurer(
			AnnotatedControllerConfigurer configurer, ApplicationContext applicationContext) {

		return new RuntimeWiringConfigurer() {
			@Override
			public void configure(RuntimeWiring.Builder builder) { }

			@Override
			public void configure(RuntimeWiring.Builder ignored, List<WiringFactory> factories) {
				try {
					configurer.setApplicationContext(createProxy(applicationContext));
					doConfigure(ignored, factories);
				}
				finally {
					configurer.setApplicationContext(applicationContext);
				}
			}

			private void doConfigure(RuntimeWiring.Builder ignored, List<WiringFactory> factories) {
				RuntimeWiring.Builder tmpRwBuilder = RuntimeWiring.newRuntimeWiring();
				configurer.configure(tmpRwBuilder);
				RuntimeWiring rw = tmpRwBuilder.build();

				Map<String, Map<String, DataFetcher>> dataFetchers = rw.getDataFetchers();

				factories.add(new WiringFactory() {
					private record TypeAndField(ObjectTypeDefinition type, FieldDefinition field) { }
					private final Map<TypeAndField, DataFetcher<?>> foundFetchers = new HashMap<>();

					@Override
					public boolean providesDataFetcher(FieldWiringEnvironment environment) {
						if ( ! (environment.getParentType() instanceof ObjectTypeDefinition typeDef))
							return false;

						if (typeDef.getImplements().isEmpty())
							return false;

						if (dataFetchers.containsKey(typeDef.getName())
								&& dataFetchers.get(typeDef.getName()).containsKey(environment.getFieldDefinition().getName()))
							return false;

						TypeAndField key = new TypeAndField(typeDef, environment.getFieldDefinition());
						findFieldOnType(key).ifPresent(df -> foundFetchers.put(key, df));
						return foundFetchers.containsKey(key);
					}

					@Override
					public DataFetcher<?> getDataFetcher(FieldWiringEnvironment environment) {
						return foundFetchers.get(
								new TypeAndField((ObjectTypeDefinition) environment.getParentType(),
										environment.getFieldDefinition()));
					}

					private Optional<DataFetcher> findFieldOnType(TypeAndField typeAndField) {
						return typeAndField.type().getImplements().stream()
								.map(ifaceType -> ((NamedNode<?>) ifaceType).getName())
								.filter(dataFetchers::containsKey)
								.map(ifaceName -> dataFetchers.get(ifaceName).get(typeAndField.field().getName()))
								.filter(Objects::nonNull)
								.findFirst();
					}
				});
			}
		};
	}

	private static ApplicationContext createProxy(ApplicationContext applicationContext) {
		ProxyFactory proxyFactory = new ProxyFactory(applicationContext);
		proxyFactory.addAdvice((MethodInterceptor) invocation -> {
			if (invocation.getMethod().getName().equals("getBean")
					&& invocation.getArguments()[0].equals(BatchLoaderRegistry.class))
				return createDummyRegistry();

			return invocation.proceed();
		});
		return (ApplicationContext) proxyFactory.getProxy();
	}

	private static BatchLoaderRegistry createDummyRegistry() {
		return ProxyFactory.getProxy(BatchLoaderRegistry.class, (MethodInterceptor) invocation -> createDummyRegistration());
	}

	private static BatchLoaderRegistry.RegistrationSpec<?, ?> createDummyRegistration() {
		return ProxyFactory.getProxy(BatchLoaderRegistry.RegistrationSpec.class, (MethodInterceptor) invocation -> null);
	}
}
