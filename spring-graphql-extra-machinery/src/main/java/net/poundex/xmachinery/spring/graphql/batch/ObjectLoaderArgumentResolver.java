package net.poundex.xmachinery.spring.graphql.batch;

import graphql.schema.DataFetchingEnvironment;
import org.dataloader.DataLoader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class ObjectLoaderArgumentResolver extends AbstractLoaderArgumentResolver {

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterType().equals(ObjectLoader.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, DataFetchingEnvironment environment) throws Exception {
		DataLoader<?, ?> dataLoader = getDataLoader(parameter, environment);
		return objectLoader(dataLoader);
	}
	
	private DataLoader<?, ?> getDataLoader(MethodParameter parameter, DataFetchingEnvironment environment) {
		String name = parameter.hasParameterAnnotation(Qualifier.class) 
				? parameter.getParameterAnnotation(Qualifier.class).value() 
				: toString(ResolvableType.forMethodParameter(parameter).getGeneric(1));
		DataLoader<Object, Object> dataLoader = environment.getDataLoader(name);

		if(dataLoader != null)
			return dataLoader;
		
		return environment.getDataLoader(parameter.getParameterName());
	}
	
	private <K, V> ObjectLoader<K, V> objectLoader(DataLoader<K, V> dataLoader) {
		return new ObjectLoader<>() {
			@Override
			public Mono<V> load(K key) {
				return Mono.fromCompletionStage(dataLoader.load(key));
			}

			@Override
			public Mono<V> load(K key, Object keyContext) {
				return Mono.fromCompletionStage(dataLoader.load(key, keyContext));
			}

			@Override
			public Mono<List<? extends V>> loadMany(Iterable<K> keys) {
				return Mono.fromCompletionStage(
						dataLoader.loadMany(keys instanceof List<K> l ? l : toList(keys)));
			}

			@Override
			public Mono<List<? extends V>> loadMany(Iterable<K> keys, Iterable<Object> keyContexts) {
				return Mono.fromCompletionStage(dataLoader.loadMany(
						keys instanceof List<K> l ? l : toList(keys),
						keyContexts instanceof List<Object> l ? l : toList(keyContexts)));
			}
		};
	}
}
