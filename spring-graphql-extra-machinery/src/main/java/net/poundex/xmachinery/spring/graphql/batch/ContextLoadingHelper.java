package net.poundex.xmachinery.spring.graphql.batch;

import graphql.schema.DataFetchingEnvironment;
import io.vavr.collection.Stream;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.poundex.xmachinery.spring.graphql.error.NotFoundException;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.graphql.data.GraphQlArgumentBinder;
import org.springframework.graphql.data.method.annotation.BatchMapping;
import org.springframework.graphql.data.method.annotation.ContextValue;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.graphql.data.method.annotation.SubscriptionMapping;
import org.springframework.graphql.data.method.annotation.support.ContextValueMethodArgumentResolver;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.Mono;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;

@Slf4j
class ContextLoadingHelper {
	
	private static final GraphQlArgumentBinder argBinder = new GraphQlArgumentBinder();
	private static final ContextValueMethodArgumentResolver contextArgResolver = 
			new ContextValueMethodArgumentResolver();
	
	@SuppressWarnings("unchecked")
	public static Object proxyController(Object bean) {
		DynamicType.Builder<Object> proxyClassDef = Stream.of(bean.getClass().getMethods())
				.filter(m -> m.isAnnotationPresent(SchemaMapping.class)
						|| m.isAnnotationPresent(QueryMapping.class)
						|| m.isAnnotationPresent(BatchMapping.class)
						|| m.isAnnotationPresent(MutationMapping.class)
						|| m.isAnnotationPresent(SubscriptionMapping.class))
				.foldLeft(
						new ByteBuddy().subclass(Object.class)
								.name(bean.getClass().getName() + "$HandlerAdapter")
								.annotateType(bean.getClass().getAnnotations()),

						(ptb, m) -> {
							DynamicType.Builder.MethodDefinition.ParameterDefinition<?> methodDef =
									ptb.defineMethod(m.getName(), m.getReturnType(), m.getModifiers());

							DynamicType.Builder.MethodDefinition.ParameterDefinition<?> methodParamsDef =
									Stream.of(m.getParameters())
											.foldLeft(methodDef, (mbb, param) ->
													mbb.withParameter(
																	param.getParameterizedType(),
																	param.getName(),
																	param.getModifiers())
															.annotateParameter(param.getAnnotations()));

							if (Arrays.stream(m.getParameterTypes())
									.noneMatch(it -> it.isAssignableFrom(DataFetchingEnvironment.class)))
								methodParamsDef = methodParamsDef.withParameter(
										DataFetchingEnvironment.class, "dataFetchingEnvironment", 0);

							return (DynamicType.Builder<Object>) methodParamsDef.intercept(
											InvocationHandlerAdapter.of((proxy, method, args) ->
													invokeOriginal(m, bean, args)))
									.annotateMethod(m.getAnnotations());
						});

		return Try.of(() -> proxyClassDef.make()
						.load(bean.getClass().getClassLoader())
						.getLoaded()
						.getConstructors()[0]
						.newInstance())
				.get();
	}

	private static Object invokeOriginal(Method origMethod, Object targetObj, Object[] args) throws InvocationTargetException, IllegalAccessException {
		ReflectionUtils.makeAccessible(origMethod);

		DataFetchingEnvironment dfe = (DataFetchingEnvironment) Arrays.stream(args)
				.filter(a -> a instanceof DataFetchingEnvironment)
				.findFirst()
				.get();
		
		Arrays.stream(origMethod.getAnnotationsByType(LoadedContextObject.class))
				.forEach(lco -> {
					String argName = lco.value().getSimpleName().toLowerCase();
					Object argValue = Try.of(() -> argBinder.bind(dfe, argName, ResolvableType.NONE)).get();
					dfe.getGraphQlContext().put(argName,
							Mono.fromCompletionStage(dfe.getDataLoaderRegistry()
									.getDataLoader(lco.value().getName())
									.load(argValue))
									.switchIfEmpty(Mono.error(() -> new NotFoundException(lco.value(), argValue.toString()))));
				});

		for (int i = 0; i < origMethod.getParameters().length; i++) {
			Parameter param = origMethod.getParameters()[i];
			if (param.isAnnotationPresent(ContextValue.class))
				args[i] = getNewContextValue(param, dfe);
		}
		
		return origMethod.invoke(targetObj, args.length == origMethod.getParameterCount()
				? args
				: Arrays.copyOfRange(args, 0, args.length - 1));
	}

	@Nullable
	private static Object getNewContextValue(Parameter param, DataFetchingEnvironment dfe) {
		Object val = contextArgResolver.resolveArgument(getMethodParameter(param), dfe);
		if(val instanceof Mono<?> m) return m.block();
		return val;
	}

	private static MethodParameter getMethodParameter(Parameter param) {
		MethodParameter mp = MethodParameter.forParameter(param);
		mp.initParameterNameDiscovery(new DefaultParameterNameDiscoverer());
		return mp;
	}
}
