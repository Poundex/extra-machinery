package net.poundex.xmachinery.spring.graphql.batch;

import reactor.core.publisher.Mono;

import java.util.List;

public interface ObjectLoader<K, V> {
	
	Mono<V> load(K key);
	Mono<V> load(K key, Object keyContext);
	
	Mono<List<? extends V>> loadMany(Iterable<K> keys);
	Mono<List<? extends V>> loadMany(Iterable<K> key, Iterable<Object> keyContexts);
}
