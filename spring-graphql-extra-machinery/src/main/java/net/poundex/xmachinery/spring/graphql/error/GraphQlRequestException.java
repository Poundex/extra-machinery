package net.poundex.xmachinery.spring.graphql.error;

import graphql.ErrorClassification;
import lombok.experimental.StandardException;

import java.util.Collections;
import java.util.Map;

@StandardException
public abstract class GraphQlRequestException extends RuntimeException {

	public abstract ErrorClassification getErrorType();

	public Map<String, Object> getExtensions() {
		return Collections.emptyMap();
	}
}
