package net.poundex.xmachinery.spring.graphql.batch;

import org.springframework.core.ResolvableType;
import org.springframework.graphql.data.method.HandlerMethodArgumentResolver;

import java.util.Arrays;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

abstract class AbstractLoaderArgumentResolver implements HandlerMethodArgumentResolver {

	protected static <K> List<K> toList(Iterable<K> keys) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
						keys.iterator(),
						Spliterator.ORDERED), false)
				.toList();
	}

	public static String toString(ResolvableType p) {
		if(p.resolve() == null)
			return "?";

		String outer = p.resolve().getName();
		if(p.hasGenerics())
			return String.format("%s<%s>", outer, Arrays.stream(p.getGenerics())
					.map(AbstractLoaderArgumentResolver::toString)
					.collect(Collectors.joining(", ")));

		return outer;
	}
}
