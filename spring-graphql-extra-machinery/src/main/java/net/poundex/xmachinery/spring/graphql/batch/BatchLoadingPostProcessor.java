package net.poundex.xmachinery.spring.graphql.batch;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.graphql.data.method.annotation.support.AnnotatedControllerConfigurer;
import org.springframework.graphql.execution.BatchLoaderRegistry;

@RequiredArgsConstructor
class BatchLoadingPostProcessor implements BeanPostProcessor {

	private final BatchLoaderRegistry batchLoaderRegistry;
	private final ObjectLoaderArgumentResolver objectLoaderArgumentResolver;
	private final UniverseLoaderArgumentResolver universeLoaderArgumentResolver;
	
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if(bean instanceof AnnotatedControllerConfigurer c)
			postProcessControllerConfigurer(c);
		
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean.getClass().getAnnotation(RegisterBatchLoaders.class) != null)
			BatchLoaderRegisterer.register(batchLoaderRegistry, bean);
		if(bean.getClass().getAnnotation(LoadContextObjects.class) != null)
			return ContextLoadingHelper.proxyController(bean);

		return bean;
	}

	private void postProcessControllerConfigurer(AnnotatedControllerConfigurer configurer) {
		configurer.addCustomArgumentResolver(objectLoaderArgumentResolver);
		configurer.addCustomArgumentResolver(universeLoaderArgumentResolver);
	}
}
