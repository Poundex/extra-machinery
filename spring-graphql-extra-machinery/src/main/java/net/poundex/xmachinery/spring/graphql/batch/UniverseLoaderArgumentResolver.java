package net.poundex.xmachinery.spring.graphql.batch;

import graphql.schema.DataFetchingEnvironment;
import org.dataloader.DataLoader;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

@Component
public class UniverseLoaderArgumentResolver extends AbstractLoaderArgumentResolver {

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterType().equals(UniverseLoader.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, DataFetchingEnvironment environment) throws Exception {
		return doResolveArgument(parameter, environment);
	}

	private <K, V> Object doResolveArgument(MethodParameter parameter, DataFetchingEnvironment environment) {
		DataLoader<Object, Map<K, V>> dataLoader = getDataLoader(parameter, environment);
		return universeLoader(dataLoader);
	}

	private <K, V> DataLoader<Object, Map<K, V>> getDataLoader(MethodParameter parameter, DataFetchingEnvironment environment) {
		Class<?> valueType = getValueType(parameter);
		if (valueType != null)
			return environment.getDataLoader(String.format("Universe<%s>", valueType.getName()));

		return environment.getDataLoader(parameter.getParameterName());
	}
	
	protected Class<?> getValueType(MethodParameter param) {
		Type genericType = param.getGenericParameterType();
		if (genericType instanceof ParameterizedType parameterizedType) {
			if (parameterizedType.getActualTypeArguments().length == 2) {
				Type valueType = parameterizedType.getActualTypeArguments()[1];
				return (valueType instanceof Class
						? (Class<?>) valueType
						: ResolvableType.forType(valueType).resolve());
			}
		}

		return null;
	}

	private <K, V> UniverseLoader<K, V> universeLoader(DataLoader<Object, Map<K, V>> dataLoader) {
		return new UniverseLoader<>() {

			@Override
			public Mono<Map<K, ? extends V>> load() {
				return Mono.fromCompletionStage(dataLoader.load(THE_KEY));
			}

			@Override
			public Mono<Map<K, ? extends V>> load(Object keyContext) {
				return Mono.fromCompletionStage(dataLoader.load(THE_KEY, keyContext));
			}
		};
	}
}
