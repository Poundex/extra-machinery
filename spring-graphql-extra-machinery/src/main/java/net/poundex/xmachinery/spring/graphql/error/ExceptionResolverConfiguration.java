package net.poundex.xmachinery.spring.graphql.error;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExceptionResolverConfiguration {
	@Bean
	public ExceptionResolver exceptionResolver(ObjectProvider<ExceptionHandler<?>> exceptionHandlers) {
		return new ExceptionResolver(exceptionHandlers.stream());
	}
}
