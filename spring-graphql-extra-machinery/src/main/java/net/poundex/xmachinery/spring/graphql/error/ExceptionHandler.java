package net.poundex.xmachinery.spring.graphql.error;

import graphql.GraphQLError;
import graphql.schema.DataFetchingEnvironment;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.BiFunction;

public record ExceptionHandler<T extends Throwable>(Class<T> klass, BiFunction<T, DataFetchingEnvironment, Mono<List<GraphQLError>>> handler) {
}
