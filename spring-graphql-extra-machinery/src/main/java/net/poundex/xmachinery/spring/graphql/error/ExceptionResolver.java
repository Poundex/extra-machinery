package net.poundex.xmachinery.spring.graphql.error;

import graphql.ErrorClassification;
import graphql.GraphQLError;
import graphql.GraphqlErrorBuilder;
import graphql.schema.DataFetchingEnvironment;
import jakarta.validation.ConstraintViolationException;
import org.springframework.graphql.execution.DataFetcherExceptionResolver;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Stream;

class ExceptionResolver implements DataFetcherExceptionResolver {
	
	private final Map<Class<Throwable>, BiFunction<Throwable, DataFetchingEnvironment, Mono<List<GraphQLError>>>> handlers;

	@SuppressWarnings("unchecked")
	public ExceptionResolver(Stream<ExceptionHandler<?>> stream) {
		this.handlers = Map.ofEntries(stream.map(ExceptionResolver::toEntry)
				.toArray(Map.Entry[]::new));
	}
	
	private static <T extends Throwable> Map.Entry<Class<T>, BiFunction<T, DataFetchingEnvironment, Mono<List<GraphQLError>>>> toEntry(ExceptionHandler<T> handler) {
		return Map.entry(handler.klass(), handler.handler());
	}

	@Override
	public Mono<List<GraphQLError>> resolveException(Throwable exception, DataFetchingEnvironment environment) {
		GraphqlErrorBuilder<?> builder = GraphqlErrorBuilder.newError(environment);
		
		if (exception instanceof GraphQlRequestException rex)
			return handleRequestException(rex, builder);
		if (exception instanceof ConstraintViolationException cvex)
			return handleValidationFailure(cvex, builder);
		
		return handlers.getOrDefault(exception.getClass(), (ig1, ig2) -> Mono.empty()).apply(exception, environment);
	}

	private static Mono<List<GraphQLError>> handleRequestException(GraphQlRequestException rex, GraphqlErrorBuilder<?> builder) {
		return Mono.just(List.of(builder
				.errorType(rex.getErrorType())
				.message(rex.getMessage())
				.extensions(rex.getExtensions())
				.build()));
	}

	private Mono<List<GraphQLError>> handleValidationFailure(ConstraintViolationException cvex, GraphqlErrorBuilder<?> builder) {
		return Mono.just(List.of(builder
				.errorType(ErrorClassification.errorClassification("UNPROCESSABLE_ENTITY"))
				.message("There are validation failures")
				.extensions(Map.of(
						"validationErrors", cvex.getConstraintViolations().stream()
								.map(cv -> Map.of(
										"type", "FIELD_ERROR",
										"message", cv.getMessage(),
										"path", cv.getPropertyPath().toString()))
								.toList()))
				.build()));
	}
}
