package net.poundex.xmachinery.spring.graphql.batch;

import graphql.schema.DataFetchingFieldSelectionSet;
import lombok.RequiredArgsConstructor;
import org.dataloader.DataLoader;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class BatchLoadingHelper {
	
	public static <T, R, K> AndMap<T, R, K> loadIfNecessary(K key, DataFetchingFieldSelectionSet fieldSelectionSet, DataLoader<K, T> loader) {
		return loadIfNecessary("id", key, fieldSelectionSet, loader);
	}
	
	public static <T, R, K> AndMap<T, R, K> loadIfNecessary(K key, DataFetchingFieldSelectionSet fieldSelectionSet, ObjectLoader<K, T> loader) {
		return loadIfNecessary("id", key, fieldSelectionSet, loader);
	}
	
	public static <T, R, K> AndMap<T, R, K> loadIfNecessary(String keyName, K key, DataFetchingFieldSelectionSet fieldSelectionSet, DataLoader<K, T> loader) {
		return new AndMap<>(keyName, key, fieldSelectionSet, new Loader.DataLoaderLoader<>(loader));
	}
	
	public static <T, R, K> AndMap<T, R, K> loadIfNecessary(String keyName, K key, DataFetchingFieldSelectionSet fieldSelectionSet, ObjectLoader<K, T> loader) {
		return new AndMap<>(keyName, key, fieldSelectionSet, new Loader.ObjectLoaderLoader<>(loader));
	}

	@RequiredArgsConstructor
	public static class AndMap<T, R, K> {
		
		private final String keyName;
		private final K key;
		private final DataFetchingFieldSelectionSet fieldSelectionSet;
		private final Loader<K, T> loader;
		
		public MapOrElse andMap(Function<T, R> mapper) {
			return new MapOrElse(mapper);
		}
		
		public OrElse andGet() {
			return new OrElse();
		}

		private Optional<Mono<T>> get() {
			if (fieldSetContainsAnythingOtherThanId())
				return Optional.of(loader.load(key));
			
			return Optional.empty();
		}
		
		private boolean fieldSetContainsAnythingOtherThanId() {
			return fieldSelectionSet.getFields().stream().anyMatch(f -> ! f.getName().equals(keyName));
		}

		@RequiredArgsConstructor
		public class MapOrElse {
			private final Function<T, R> mapper;
			
			@SuppressWarnings("unchecked")
			public <RR extends R> Mono<RR> orElse(Supplier<RR> supplier) {
				return (Mono<RR>) get().map(m -> m.map(mapper))
						.orElseGet(() -> Mono.fromSupplier(supplier));
			}
		}

		@RequiredArgsConstructor
		public class OrElse {

			@SuppressWarnings("unchecked")
			public <RR extends T> Mono<RR> orElse(Supplier<RR> supplier) {
				return (Mono<RR>) get()
						.orElseGet(() -> Mono.fromSupplier(supplier));
			}
		}
	}
	
	private static abstract class Loader<K, V> {
		abstract Mono<V> load(K key);
		@RequiredArgsConstructor
		static class DataLoaderLoader<K, V> extends Loader<K, V> {
			private final DataLoader<K, V> dataLoader;

			@Override
			Mono<V> load(K key) {
				return Mono.fromCompletionStage(dataLoader.load(key));
			}
		}
		
		@RequiredArgsConstructor
		static class ObjectLoaderLoader<K, V> extends Loader<K, V> {
			private final ObjectLoader<K, V> objectLoader;

			@Override
			Mono<V> load(K key) {
				return objectLoader.load(key);
			}
		}
	}

}
