package net.poundex.xmachinery.spring.graphql.batch;

import reactor.core.publisher.Mono;

import java.util.Map;

public interface UniverseLoader<K, V> {
	Object THE_KEY = new Object();
	
	Mono<Map<K, ? extends V>> load();
	Mono<Map<K, ? extends V>> load(Object keyContext);
}
