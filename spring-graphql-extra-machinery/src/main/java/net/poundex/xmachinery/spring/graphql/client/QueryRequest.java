package net.poundex.xmachinery.spring.graphql.client;

import com.netflix.graphql.dgs.client.codegen.BaseProjectionNode;
import com.netflix.graphql.dgs.client.codegen.GraphQLMultiQueryRequest;
import com.netflix.graphql.dgs.client.codegen.GraphQLQuery;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;

import java.util.ArrayList;
import java.util.List;

public class QueryRequest {

	private record QueryRequestPair(GraphQLQuery query, BaseProjectionNode projection) {}

	private final List<QueryRequestPair> requestPairs = new ArrayList<>();

	public static QueryRequest of(GraphQLQuery query, BaseProjectionNode projection) {
		return new QueryRequest(query, projection);
	}

	public static QueryRequest of(QueryRequest queryRequest) {
		return new QueryRequest(queryRequest);
	}
	
	public QueryRequest(GraphQLQuery query, BaseProjectionNode projection) {
		this.requestPairs.add(new QueryRequestPair(query, projection));
	}

	public QueryRequest(QueryRequest queryRequest) {
		this.requestPairs.addAll(queryRequest.requestPairs);
	}

	public QueryRequest and(GraphQLQuery query, BaseProjectionNode projection) {
		this.requestPairs.add(new QueryRequestPair(query, projection));
		return this;
	}
	
	public QueryRequest and(QueryRequest other) {
		requestPairs.addAll(other.requestPairs);
		return this;
	}
	
	public String serialize() {
		List<GraphQLQueryRequest> requests = requestPairs.stream()
				.map(pair -> new GraphQLQueryRequest(pair.query(), pair.projection()))
				.toList();

		if(requests.size() == 1)
			return requests.getFirst().serialize();
		
		return new GraphQLMultiQueryRequest(requests).serialize();
	}
}
