package net.poundex.xmachinery.spring.graphql.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import java.util.Map;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseFixingGraphQlClientDecorator implements GraphQlClient {

	public static GraphQlClient wrap(MonoGraphQLClient client, ObjectMapper objectMapper) {
		return new ResponseFixingGraphQlClientDecorator(client, objectMapper);
	}
	
	private final MonoGraphQLClient delegate;
	private final ObjectMapper objectMapper;
	
	@Override
	public Mono<QueryResponse> query(String query) {
		return delegate.reactiveExecuteQuery(query)
				.map(r -> QueryResponse.wrap(r, objectMapper));
	}

	@Override
	public Mono<QueryResponse> query(String query, Map<String, ?> map) {
		return delegate.reactiveExecuteQuery(query, map)
				.map(r -> QueryResponse.wrap(r, objectMapper));
	}

	@Override
	public Mono<QueryResponse> query(String query, Map<String, ?> map, String operationName) {
		return delegate.reactiveExecuteQuery(query, map, operationName)
				.map(r -> QueryResponse.wrap(r, objectMapper));
	}
}
