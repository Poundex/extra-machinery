package net.poundex.xmachinery.spring.graphql.batch;

import graphql.GraphQLContext;
import io.vavr.control.Try;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.dataloader.BatchLoaderEnvironment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.graphql.execution.BatchLoaderRegistry;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.Mono;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unchecked")
class BatchLoaderRegisterer {
	
	private interface BatchLoaderInvoker<K, V> {
		Class<K> getIdType();
		Class<V> getValueType();
		Mono<Map<K, V>> invoke(Set<K> ids, BatchLoaderEnvironment env);

		String getName();
	}
	
	public static <K, V> void register(BatchLoaderRegistry batchLoaderRegistry, Object bean) {
		MethodUtils.getMethodsListWithAnnotation(bean.getClass(), BatchLoader.class, false, true)
				.stream()
				.map(m -> BatchLoaderRegisterer.<K, V>toBatchLoaderInvoker(bean, m))
				.forEach(invoker -> batchLoaderRegistry.forTypePair(
								invoker.getIdType(),
								invoker.getValueType())
						.withName(invoker.getName())
						.registerMappedBatchLoader(invoker::invoke));
	}

	private static <K, V> BatchLoaderInvoker<K, V> toBatchLoaderInvoker(Object receiver, Method method) {
		ResolvableType[] mapKeyAndValueTypes = ResolvableType
				.forMethodReturnType(method)
				.getGenerics()[0]
				.getGenerics();
		
		ReflectionUtils.makeAccessible(method);
		if(method.getParameterCount() == 0 || Arrays.stream(method.getParameters())
				.noneMatch(param -> {
					ResolvableType type = ResolvableType.forMethodParameter(MethodParameter.forParameter(param));
					return type.getRawClass().equals(Set.class) 
							&& type.getGeneric(0).equals(mapKeyAndValueTypes[0]);
				}))
			return (BatchLoaderInvoker<K, V>) BatchLoaderRegisterer.<V>createUniverseLoader(receiver, method, mapKeyAndValueTypes[1]);

		return createObjectLoader(receiver, method, mapKeyAndValueTypes);
	}

	@NotNull
	private static <K, V> BatchLoaderInvoker<K, V> createObjectLoader(Object receiver, Method method, ResolvableType[] batchLoadTypes) {
		return new BatchLoaderInvoker<>() {
			@Override
			public Class<K> getIdType() {
				return (Class<K>) batchLoadTypes[0].resolve();
			}

			@Override
			public Class<V> getValueType() {
				return (Class<V>) batchLoadTypes[1].resolve();
			}

			@Override
			public Mono<Map<K, V>> invoke(Set<K> ids, BatchLoaderEnvironment env) {
				return (Mono<Map<K, V>>) Try.of(() -> method.invoke(receiver,
						getInvokeArgs(method, ids, env))).get();
			}

			@Override
			public String getName() {
				Qualifier ann = AnnotationUtils.findAnnotation(method, Qualifier.class);
				return ann != null 
						? method.getName() 
						: AbstractLoaderArgumentResolver.toString(batchLoadTypes[1]);
			}
		};
	}

	private static <V> BatchLoaderInvoker<?, Map<?, V>> createUniverseLoader(Object receiver, Method method, ResolvableType valueType) {
		return new BatchLoaderInvoker<>() {
			@Override
			public Class<Object> getIdType() {
				return Object.class;
			}

			@Override
			public Class<Map<?, V>> getValueType() {
				return (Class<Map<?, V>>) TypeDescriptor.map(
						Map.class, 
						TypeDescriptor.valueOf(Object.class), 
						TypeDescriptor.valueOf(valueType.resolve()))
						.getType();
			}

			@Override
			public Mono<Map<Object, Map<?, V>>> invoke(Set<Object> ids, BatchLoaderEnvironment env) {
				return (Try.of(() -> (Mono<Map<?, V>>) method.invoke(
						receiver,
						getInvokeArgs(method, ids, env))).get())
						.map(set -> Map.of(UniverseLoader.THE_KEY, set));
			}

			@Override
			public String getName() {
				return String.format("Universe<%s>", valueType.resolve().getName());
			}
		};
	}

	private static <K> Object[] getInvokeArgs(Method method, Set<K> ids, BatchLoaderEnvironment env) {
		return Arrays.stream(method.getParameters())
				.map(param -> {
					if (param.getType().isAssignableFrom(Set.class))
						return ids;
					if (param.getType().isAssignableFrom(BatchLoaderEnvironment.class))
						return env;
					if (env.<GraphQLContext>getContext().hasKey(param.getName()))
						return env.<GraphQLContext>getContext().get(param.getName());
					return null;
				})
				.toArray();
	}
}
