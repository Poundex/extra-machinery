package net.poundex.xmachinery.spring.graphql.client;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.graphql.dgs.client.GraphQLResponse;
import io.vavr.Lazy;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class QueryResponse {
	public static QueryResponse wrap(GraphQLResponse r, ObjectMapper objectMapper) {
		return new QueryResponse(r.getJson(), objectMapper);
	}
	
	private final String json;
	private final ObjectMapper objectMapper;
	private final Lazy<JsonNode> rootNode = Lazy.of(this::getRootNode);

	private JsonNode getRootNode() {
		return Try.of(() -> objectMapper.readTree(json).get("data")).get();
	}

	public <T> T extractValue(String path, Class<T> klass) {
		return Try.of(() -> {
			JsonNode node = rootNode.get().get(path);
			return objectMapper.treeToValue(node, klass);
		}).get();
	}
	
	public <T> T extractValue(Class<T> klass) {
		return Try.of(() -> {
			JsonNode node = rootNode.get().iterator().next();
			return objectMapper.treeToValue(node, klass);
		}).get();
	}

	public <T> List<T> extractList(String path, Class<T> klass) {
		return (List<T>) Try.of(() -> {
			JavaType type = objectMapper.getTypeFactory().
					constructCollectionType(List.class, klass);
			
			JsonNode node = rootNode.get().get(path);
			return objectMapper.treeToValue(node, type);
		}).get();
	}
	
	public <T> List<T> extractList(Class<T> klass) {
		return (List<T>) Try.of(() -> {
			JavaType type = objectMapper.getTypeFactory().
					constructCollectionType(List.class, klass);

			JsonNode node = rootNode.get().iterator().next();
			return objectMapper.treeToValue(node, type);
		}).get();
	}
}
