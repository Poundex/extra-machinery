package net.poundex.xmachinery.spring.graphql.client;

import com.netflix.graphql.dgs.client.codegen.BaseProjectionNode;
import com.netflix.graphql.dgs.client.codegen.GraphQLQuery;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.intellij.lang.annotations.Language;
import reactor.core.publisher.Mono;

import java.util.Map;

public interface GraphQlClient {
	Mono<QueryResponse> query(@Language("graphql") String query);
	Mono<QueryResponse> query(@Language("graphql") String query, Map<String, ?> map);
	Mono<QueryResponse> query(@Language("graphql") String query, Map<String, ?> map, String operationName);
	
	default Mono<QueryResponse> query(GraphQLQuery query, BaseProjectionNode projection) {
		return query(new GraphQLQueryRequest(query, projection).serialize());
	}
	
	default Mono<QueryResponse> query(QueryRequest queryRequest) {
		return query(queryRequest.serialize());
	}
}
