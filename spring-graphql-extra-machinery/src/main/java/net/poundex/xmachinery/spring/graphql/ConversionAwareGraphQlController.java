package net.poundex.xmachinery.spring.graphql;

import graphql.GraphQLContext;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Deprecated(forRemoval = true)
public abstract class ConversionAwareGraphQlController<T> {

	protected  <S extends T> void addSourceObject(GraphQLContext ctx, S value, Function<S, ?> idExtractor) {
		if( ! ctx.hasKey("source"))
			ctx.put("source", new HashMap<String, T>());

		((Map<Object, S>) ctx.get("source")).put(idExtractor.apply(value), value);
	}

	protected <S extends T> S getSourceObject(GraphQLContext ctx, String id) {
		return ((Map<Object, ? extends S>) ctx.get("source")).get(id);
	}
}
