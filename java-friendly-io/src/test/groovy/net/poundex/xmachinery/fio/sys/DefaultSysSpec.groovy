package net.poundex.xmachinery.fio.sys

import spock.lang.Specification
import spock.lang.Subject

class DefaultSysSpec extends Specification {
    
    @Subject
    Sys sys = new DefaultSys()
    
    void "Returns environment variable if found or empty"() {
        expect:
        sys.env("DOES_NOT_EXIST") == Optional.empty()
        sys.env("HOME").get().startsWith("/")
    }
    
    void "Returns JVM prop if found or empty"() {
        expect:
        sys.prop("does.not.exist") == Optional.empty()
        sys.prop("user.dir").get().startsWith("/")
    }
}
