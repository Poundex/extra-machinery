package net.poundex.xmachinery.fio.io

import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class DefaultIoSpec extends Specification {
    
    @Subject
    IO io = new DefaultIo()

    void "ensureDirectory creates when does not exist"() {
        given:
        Path target = Paths.get("/tmp/create-this-dir")
        
        when:
        Path ret = io.createDirectories(target)
        
        then:
        ret == target
        Files.isDirectory(target)
        
        cleanup:
        Files.delete(target)
    }
    
    void "ensureDirectory does nothing when dir exists"() {
        given:
        Path target = Paths.get("/tmp/create-this-dir")
        Files.createDirectories(target)

        when:
        Path ret = io.createDirectories(target)

        then:
        ret == target
        Files.isDirectory(target)

        cleanup:
        Files.delete(target)
    }
    
    void "ensureDirectory throws when can't create directory"() {
        given:
        Path target = Paths.get("/tmp/create-this-dir")
        Files.createFile(target)

        when:
        Path ret = io.createDirectories(target)

        then:
        thrown(UncheckedIOException)

        cleanup:
        Files.delete(target)
    }
    
    void "Runs function with reader"() {
        Path file = Files.createTempFile("test", ".txt")
        given:
        Files.write(file, ["test-string"])
        
        expect:
        io.withReader(file, r -> r.readLines().first()) == "test-string"
    }
    
    void "Runs function with writer"() {
        Path file = Files.createTempFile("test", ".txt")

        when:
        io.withWriter(file, w -> w.write("test-string"))
        
        then:
        Files.readString(file) == "test-string"
    }
}
