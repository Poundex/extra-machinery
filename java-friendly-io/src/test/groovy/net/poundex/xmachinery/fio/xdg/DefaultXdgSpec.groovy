package net.poundex.xmachinery.fio.xdg


import net.poundex.xmachinery.fio.sys.Sys
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Paths

class DefaultXdgSpec extends Specification {
    
    private static final String APP_NAME = "appname"

    Sys sys = Stub()
            
    @Subject
    Xdg xdg = new DefaultXdg(sys)
    
    void "Returns correct configured directory"() {
        given:
        sys.env("XDG_CONFIG_HOME") >> Optional.of("/path/to/config")
        sys.env("XDG_CACHE_HOME") >> Optional.of("/path/to/cache")
        sys.env("XDG_DATA_HOME") >> Optional.of("/path/to/data")
        sys.env("XDG_STATE_HOME") >> Optional.of("/path/to/state")
        
        expect:
        xdg.getConfigDirectory(APP_NAME) == Paths.get("/path/to/config/${APP_NAME}")
        xdg.getCacheDirectory(APP_NAME) == Paths.get("/path/to/cache/${APP_NAME}")
        xdg.getDataDirectory(APP_NAME) == Paths.get("/path/to/data/${APP_NAME}")
        xdg.getStateDirectory(APP_NAME) == Paths.get("/path/to/state/${APP_NAME}")
    }

    
    void "Returns correct default directory"() {
        given:
        sys.env("HOME") >> Optional.of("/user/home")

        expect:
        xdg.getConfigDirectory(APP_NAME) == Paths.get("/user/home/.config/${APP_NAME}")
        xdg.getCacheDirectory(APP_NAME) == Paths.get("/user/home/.cache/${APP_NAME}")
        xdg.getDataDirectory(APP_NAME) == Paths.get("/user/home/.local/share/${APP_NAME}")
        xdg.getStateDirectory(APP_NAME) == Paths.get("/user/home/.local/state/${APP_NAME}")
    }
}
