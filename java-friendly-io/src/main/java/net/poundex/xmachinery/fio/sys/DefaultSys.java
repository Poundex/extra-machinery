package net.poundex.xmachinery.fio.sys;

import java.util.Optional;

public class DefaultSys implements Sys {

    @Override
    public Optional<String> env(String name) {
        String r = System.getenv(name);
        if(r == null || r.trim().isEmpty())
            return Optional.empty();
        return Optional.of(r);
    }

    @Override
    public Optional<String> prop(String name) {
        String r = System.getProperty(name);
        if(r == null || r.trim().isEmpty())
            return Optional.empty();
        return Optional.of(r);
    }
}
