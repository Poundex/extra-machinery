package net.poundex.xmachinery.fio.io;

import java.io.IOException;

@FunctionalInterface
public interface IoCheckedConsumer<T> {
	void accept(T t) throws IOException;
}
