package net.poundex.xmachinery.fio.xdg;

import lombok.RequiredArgsConstructor;
import net.poundex.xmachinery.fio.sys.Sys;

import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
public class DefaultXdg implements Xdg {
    
    private final Sys sys;
    
    @Override
    public Path getConfigDirectory(String applicationName) {
        return getDirectory("XDG_CONFIG_HOME", ".config/", applicationName);
    }

    @Override
    public Path getCacheDirectory(String applicationName) {
        return getDirectory("XDG_CACHE_HOME", ".cache/", applicationName);
    }

    @Override
    public Path getDataDirectory(String applicationName) {
        return getDirectory("XDG_DATA_HOME", ".local/share/", applicationName);
    }

    @Override
    public Path getStateDirectory(String applicationName) {
        return getDirectory("XDG_STATE_HOME", ".local/state/", applicationName);
    }

    private Path getDirectory(String envVar, String defaultPath, String applicationName) {
        return sys.env(envVar)
                .map(Paths::get)
                .or(() -> sys.env("HOME")
                        .map(Paths::get)
                        .map(p -> p.resolve(defaultPath)))
                .map(p -> p.resolve(applicationName))
                .orElseThrow();
    }
}
