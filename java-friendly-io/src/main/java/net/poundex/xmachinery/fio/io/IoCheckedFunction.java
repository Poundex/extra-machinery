package net.poundex.xmachinery.fio.io;

import java.io.IOException;

@FunctionalInterface
public interface IoCheckedFunction<T, R> {
	R apply(T t) throws IOException;
}
