package net.poundex.xmachinery.fio.io;


import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.stream.Stream;

public class DefaultIo implements IO {
    
    @Override
    public <T> T withReader(Path path, IoCheckedFunction<Reader, T> fn) {
        try(Reader r = Files.newBufferedReader(path)) {
            return fn.apply(r);
        } catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }

    @Override
    public Path createDirectories(Path path) {
        try {
            Files.createDirectories(path);
        } catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
        return path;
    }

    @Override
    public void withWriter(Path path, IoCheckedConsumer<Writer> fn) {
        try(Writer w = Files.newBufferedWriter(path)) {
            fn.accept(w);
        } catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }

    @Override
    public Path createTempFile(String prefix, String suffix, FileAttribute<?>... attrs) {
        try {
            return Files.createTempFile(prefix, suffix, attrs);
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }

    @Override
    public BufferedReader newBufferedReader(Path path) {
        try {
            return Files.newBufferedReader(path);
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }

    @Override
    public void close(Closeable closeable) {
        try {
            closeable.close();
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }

    @Override
    public Path writeString(Path path, CharSequence csq, OpenOption... options) {
        try {
            return Files.writeString(path, csq, options);
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }
    
    @Override
    public String readString(Path path) {
        try {
            return Files.readString(path);
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }

    @Override
    public Stream<String> lines(Path path) {
        try {
            return Files.lines(path);
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }    
    }

    @Override
    public Stream<Path> list(Path path) {
        try {
            return Files.list(path);
        }
        catch (IOException ioex) {
            throw new UncheckedIOException(ioex);
        }
    }
}
