package net.poundex.xmachinery.fio.spring;

import net.poundex.xmachinery.fio.io.DefaultIo;
import net.poundex.xmachinery.fio.io.IO;
import net.poundex.xmachinery.fio.sys.DefaultSys;
import net.poundex.xmachinery.fio.sys.Sys;
import net.poundex.xmachinery.fio.xdg.DefaultXdg;
import net.poundex.xmachinery.fio.xdg.Xdg;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FioConfiguration {
	@Bean
	public Sys sys() {
		return new DefaultSys();
	}
	
	@Bean
	public Xdg xdg(Sys sys) {
		return new DefaultXdg(sys);
	}
	
	@Bean
	public IO io() {
		return new DefaultIo();
	}
}
