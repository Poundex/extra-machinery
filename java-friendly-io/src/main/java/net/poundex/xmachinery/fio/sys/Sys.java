package net.poundex.xmachinery.fio.sys;

import java.util.Optional;

public interface Sys {
    Optional<String> env(String name);
    Optional<String> prop(String name);
}
