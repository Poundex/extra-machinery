package net.poundex.xmachinery.fio.io;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.stream.Stream;

public interface IO {
    <T> T withReader(Path path, IoCheckedFunction<Reader, T> fn);
    Path createDirectories(Path path);
    void withWriter(Path path, IoCheckedConsumer<Writer> fn);
	Path createTempFile(String prefix, String suffix, FileAttribute<?>... attrs);
	BufferedReader newBufferedReader(Path path);
	void close(Closeable closeable);
	Path writeString(Path path, CharSequence csq, OpenOption... options);
	String readString(Path path);
	Stream<String> lines(Path path);
	Stream<Path> list(Path path);
}
