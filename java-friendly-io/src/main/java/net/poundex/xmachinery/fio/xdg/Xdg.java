package net.poundex.xmachinery.fio.xdg;

import java.nio.file.Path;

public interface Xdg {
    Path getConfigDirectory(String applicationName);
    Path getCacheDirectory(String applicationName);
    Path getDataDirectory(String applicationName);
    Path getStateDirectory(String applicationName);
}
